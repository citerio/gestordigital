package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cortez.citerio.com.gestordigital.model.Preference
import kotlinx.android.synthetic.main.user_info_screen.*

class UserInfo_Screen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_info_screen)

        change_password.setOnClickListener{
            val intent = Intent(this@UserInfo_Screen, ChangePassword_Screen::class.java)
            startActivity(intent)
        }

        change_secret_questions.setOnClickListener{
            val intent = Intent(this@UserInfo_Screen, ChangeSecretQuestion_Screen::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@UserInfo_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@UserInfo_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }
}
