package cortez.citerio.com.gestordigital

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.detail_invoice_screen.*

class InvoiceDetail_ScreenBU : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_invoice_screen)

        provider.text = intent.getStringExtra("name")
        amount.text = intent.getStringExtra("amount")
    }
}
