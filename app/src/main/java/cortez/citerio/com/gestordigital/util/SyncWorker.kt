package cortez.citerio.com.gestordigital.util

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.work.Result
import androidx.work.Worker
import androidx.work.WorkerParameters
import cortez.citerio.com.gestordigital.EndPoints
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.Category
import cortez.citerio.com.gestordigital.model.Institution
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import org.json.JSONArray

class SyncWorker (context: Context, params: WorkerParameters) : Worker (context, params){

    private val TAG = "SistemaVirtualApp"
    private val service = ServiceVolley()
    private val apiController = APIController(service)
    private var database: AppDatabase? = null

    override fun doWork(): Result {

        return try {
            getCategoriesAndInstitutions()
            Result.success()
        }catch (ex: Exception){
            Log.v(TAG, "Error seeding categories and institutions in DB")
            Result.failure()
        }

    }

    fun getCategoriesAndInstitutions() {
        Log.e("isUpdated", Preference.getBoolean(applicationContext, "isUpdated").toString())
        apiController.get(EndPoints.URL_GET_INST_AND_CATE, applicationContext,
                {response ->
                    val categories = response?.getJSONArray("categories")
                    val institutions = response?.getJSONArray("institutions")
                    Log.i("categories", "$categories")
                    Log.i("institutions", "$institutions")
                    addCategoriesAndInstitutions(categories!!, institutions!!)
                },
                {_ ->
                    Toast.makeText(applicationContext, "Ha ocurrido un error obteniendo las categorias. Intente nuevamente",
                            Toast.LENGTH_SHORT).show();
                })
    }

    fun addCategoriesAndInstitutions(categories: JSONArray, institutions: JSONArray) {

        object : AsyncTask<String, Void, String>() {

            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {
                    var cates : ArrayList<Category> = ArrayList<Category>()
                    var insts : ArrayList<Institution> = ArrayList<Institution>()

                    for (i in 0 until categories!!.length()) {
                        val id = categories.getJSONObject(i).getString("_id")
                        val name = categories.getJSONObject(i).getString("name")

                        val cate = Category(id = id, name = name)
                        cates.add(cate)
                    }

                    for (i in 0 until institutions!!.length()) {
                        val id = institutions.getJSONObject(i).getString("_id")
                        val name = institutions.getJSONObject(i).getString("name")
                        val category_id = institutions.getJSONObject(i).getString("institutionCategoryId")

                        val inst = Institution(id = id, name = name, category_id = category_id)
                        insts.add(inst)
                    }

                    val database = AppDatabase.getDatabase(context = applicationContext)
                    //database?.categoryDao()?.removeAllCategories()
                    //database?.institutionDao()?.removeAllInstitutions()
                    database?.categoryDao()?.addCategories(categories = cates)
                    database?.institutionDao()?.addInstitutions(institutions = insts)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {
                        Preference.saveBoolean(applicationContext, "isUpdated", true)
                        Log.v(TAG, "categories inserted in DB")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting categories in DB")
                    Toast.makeText(applicationContext, "ERROR inside adding categories", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")
    }
}