package cortez.citerio.com.gestordigital.model

data class Transaction constructor(
    var id: String = "",
    var number: String = "",
    var date: String = "",
    var dueDate: String = "",
    var description: String = "",
    var instCat: Int = 0,
    var instName: String = "",
    var amount: String = "0.0",
    var status: Int = 0,
    var days_left: Int = 0
    )
{

}