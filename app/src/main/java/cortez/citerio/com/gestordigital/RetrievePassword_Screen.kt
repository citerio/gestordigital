package cortez.citerio.com.gestordigital

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import android.widget.Toast
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.retrieve_password_screen.*
import org.json.JSONArray
import org.json.JSONObject


class RetrievePassword_Screen : AppCompatActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.retrieve_password_screen)

        password?.addTextChangedListener(MyTextWatcher(password))
        code?.addTextChangedListener(MyTextWatcher(code))

        retrieve_password_button.setOnClickListener {
            progressbar.visibility = View.VISIBLE
            retrieve_password_button.isEnabled = false

            var valid = true
            if (TextUtils.isEmpty(code.text.toString().trim())) {
                code_layout?.error = getString(R.string.empty_field)
                code_layout?.isErrorEnabled = true
                valid = false
            }
            if (TextUtils.isEmpty(password.text.toString().trim())) {
                password_layout?.error = getString(R.string.empty_field)
                password_layout?.isErrorEnabled = true
                valid = false
            }

            if (valid) {
                val params = JSONObject()
                params.put("email", intent.getStringExtra("email"))
                params.put("password", password?.text.toString().trim())
                params.put("code", code?.text.toString().trim())

                apiController.post(EndPoints.URL_CREATE_NEW_PASSWORD, this@RetrievePassword_Screen, params,
                    { response ->
                        //progressbar.visibility = View.GONE
                        val result = response?.getString("result")
                        if (result == "OK") {
                            showDialogSuccess()
                        } else {
                            progressbar.visibility = View.GONE
                            retrieve_password_button?.isEnabled = true
                            Toast.makeText(this@RetrievePassword_Screen, "Ha ocurrido un error. Intente nuevamente.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    },
                    { error ->
                        val networkResponse = error?.networkResponse
                        var jsonError: String = ""
                        var jsonObject: JSONObject? = null
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = String(networkResponse.data)
                            jsonObject = JSONObject(jsonError)

                            if(jsonObject?.has("errors")) {
                                showServerErrors(jsonObject?.getJSONArray("errors"))
                            } else {
                                Toast.makeText(this@RetrievePassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            // Print Error!
                        } else {
                            Toast.makeText(this@RetrievePassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        retrieve_password_button?.isEnabled = true
                        progressbar.visibility = View.GONE
                    })
            } else {
                progressbar.visibility = View.GONE
                retrieve_password_button?.isEnabled = true
            }

        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        /*val currentUser = mAuth?.getCurrentUser()

        if(currentUser != null) {
            val intentToMain = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }*/
    }



    private fun showDialogSuccess() {

        AlertDialog.Builder(this@RetrievePassword_Screen)
                .setTitle("Exito")
                .setMessage("Su contraseña ha sido cambiada satisfactoriamente")
                .setCancelable(false)
                .setPositiveButton(resources.getString(R.string.accept)) { _, _ ->
                    progressbar.visibility = View.GONE
                    val signin_screen = Intent(this@RetrievePassword_Screen, SignIn_Screen::class.java)
                    signin_screen.putExtra("email", intent.getStringExtra("email"))
                    signin_screen.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(signin_screen)
                    finish()
                }
                .show()
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (wrong_credentials.isShown){
                wrong_credentials.visibility = View.GONE
            }
        }

        override fun afterTextChanged(s: Editable) {
            Log.e("info", "paso por aqui")
            when (view.id) {
                R.id.code -> validate_code()
                R.id.password -> validate_password()
            }

        }
    }

    private fun validate_code(): Boolean {

        if (code.text.toString().isEmpty()) {

            code_layout?.error = getString(R.string.empty_field)
            code_layout?.isErrorEnabled = true
            return false

        } else {
            code_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_password(): Boolean {

        if (password.text.toString().isEmpty()) {

            password_layout?.error = getString(R.string.empty_field)
            password_layout?.isErrorEnabled = true
            return false

        } else {
            password_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun showServerErrors(errors: JSONArray) {
        code_layout?.isErrorEnabled = false
        password_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }
}
