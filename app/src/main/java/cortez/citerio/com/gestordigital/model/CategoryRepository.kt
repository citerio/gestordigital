package cortez.citerio.com.gestordigital.model

class CategoryRepository private constructor(private val categoryDao: CategoryDao){


    fun getCategories() = categoryDao.getCategories()

    fun addCategories(categoryList: List<Category>){
        categoryDao.addCategories(categoryList)
    }

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: CategoryRepository? = null

        fun getInstance(categoryDao: CategoryDao) =
                instance ?: synchronized(this) {
                    instance ?: CategoryRepository(categoryDao).also { instance = it }
                }
    }
}