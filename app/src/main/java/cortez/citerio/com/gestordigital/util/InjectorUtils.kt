package cortez.citerio.com.gestordigital.util

import android.content.Context
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.viewmodel.CategoryViewModelFactory
import cortez.citerio.com.gestordigital.viewmodel.InstitutionViewModelFactory
import cortez.citerio.com.gestordigital.viewmodel.MyInstitutionViewModelFactory
import cortez.citerio.com.gestordigital.viewmodel.MyInvoiceViewModelFactory
import cortez.citerio.com.gestordigital.volley.ServiceVolley

object InjectorUtils {

    private fun getCategoryRepository(context: Context) : CategoryRepository{
        return CategoryRepository.getInstance(AppDatabase.getDatabase(context)!!.categoryDao())
    }

    fun provideCategoryViewModelFactory(context: Context) : CategoryViewModelFactory{
        val repository = getCategoryRepository(context)
        return CategoryViewModelFactory(repository)
    }

    private fun getInstitutionRepository(context: Context): InstitutionRepository{
        return InstitutionRepository.getInstance(AppDatabase.getDatabase(context)!!.institutionDao())
    }

    fun provideInstitutionViewModelFactory(context: Context) : InstitutionViewModelFactory{
        val repository = getInstitutionRepository(context)
        return InstitutionViewModelFactory(repository)
    }

    private fun getMyInstitutionRepository(context: Context): MyInstitutionRepository{
        return MyInstitutionRepository.getInstance(AppDatabase.getDatabase(context)!!.myInstitutionsDao())
    }

    fun provideMyInstitutionViewModelFactory(context: Context): MyInstitutionViewModelFactory{
        val repository = getMyInstitutionRepository(context)
        return MyInstitutionViewModelFactory(repository)
    }

    private fun getMyInvoiceRepository(context: Context): MyInvoiceRepository{
        val service = ServiceVolley()
        return MyInvoiceRepository.getInstance(AppDatabase.getDatabase(context)!!.myInvoicesDao(), service)
    }

    fun provideMyInvoiceViewModelFactory(context: Context): MyInvoiceViewModelFactory{
        val repository = getMyInvoiceRepository(context)
        return MyInvoiceViewModelFactory(repository)
    }


}
