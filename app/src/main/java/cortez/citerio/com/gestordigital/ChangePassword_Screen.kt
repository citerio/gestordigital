package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.change_password_screen.*
import org.json.JSONArray
import org.json.JSONObject


class ChangePassword_Screen : BaseActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password_screen)

        old_password?.addTextChangedListener(MyTextWatcher(old_password))
        new_password?.addTextChangedListener(MyTextWatcher(new_password))
        retype_new_password?.addTextChangedListener(MyTextWatcher(retype_new_password))

        update_button.setOnClickListener {
            progressbar.visibility = View.VISIBLE
            update_button.isEnabled = false
            cancel_button.isEnabled = false

            var valid = true
            if (TextUtils.isEmpty(old_password.text.toString().trim())) {
                old_password_layout?.error = getString(R.string.empty_field)
                old_password_layout?.isErrorEnabled = true
                valid = false
            }
            if (TextUtils.isEmpty(new_password.text.toString().trim())) {
                new_password_layout?.error = getString(R.string.empty_field)
                new_password_layout?.isErrorEnabled = true
                valid = false
            }
            if (TextUtils.isEmpty(retype_new_password.text.toString().trim())) {
                retype_new_password_layout?.error = getString(R.string.empty_field)
                retype_new_password_layout?.isErrorEnabled = true
                valid = false
            }
            if (!new_password.text.toString().trim().equals(retype_new_password.text.toString().trim())) {
                wrong_credentials.text = resources.getString(R.string.wrong_passwords)
                wrong_credentials.visibility = View.VISIBLE
                valid = false
            }

            if (valid) {
                val params = JSONObject()
                params.put("old_password", old_password.text.toString().trim())
                params.put("new_password", new_password.text.toString().trim())

                apiController.post(EndPoints.URL_CHANGE_PASSWORD, this@ChangePassword_Screen, params,
                        { response ->
                            //progressbar.visibility = View.GONE
                            val result = response?.getString("result")
                            if (result == "OK") {
                                showDialogSuccess()
                            } else {
                                progressbar.visibility = View.GONE
                                update_button?.isEnabled = true
                                cancel_button?.isEnabled = true
                                Toast.makeText(this@ChangePassword_Screen, "Ha ocurrido un error. Intente nuevamente.",
                                        Toast.LENGTH_SHORT).show();
                            }

                        },
                        { error ->
                            val networkResponse = error?.networkResponse
                            var jsonError: String = ""
                            var jsonObject: JSONObject? = null
                            if (networkResponse != null && networkResponse.data != null) {
                                jsonError = String(networkResponse.data)
                                jsonObject = JSONObject(jsonError)

                                if(jsonObject?.has("errors")) {
                                    showServerErrors(jsonObject?.getJSONArray("errors"))
                                } else {
                                    Toast.makeText(this@ChangePassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                            Toast.LENGTH_SHORT).show();
                                }
                                // Print Error!
                            } else {
                                Toast.makeText(this@ChangePassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            update_button?.isEnabled = true
                            cancel_button?.isEnabled = true
                            progressbar.visibility = View.GONE
                        })
            } else {
                progressbar.visibility = View.GONE
                update_button?.isEnabled = true
                cancel_button?.isEnabled = true
            }

        }

        cancel_button.setOnClickListener {
            finish()
        }
    }

    /*public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        /*val currentUser = mAuth?.getCurrentUser()

        if(currentUser != null) {
            val intentToMain = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }*/
    }*/

    private fun showDialogSuccess() {
        val dialogView = layoutInflater.inflate(R.layout.success_payment, null)
        val alert = AlertDialog.Builder(this@ChangePassword_Screen)
        alert.setView(dialogView)
        val dialog = alert.create()
        dialog.setCancelable(false)

        val message = dialogView.findViewById<TextView>(R.id.message)

        message?.text = getString(R.string.success_pass_change)
        dialogView.findViewById<Button>(R.id.accept).setOnClickListener {
            dialog.dismiss()
            progressbar.visibility = View.GONE
            finish()
        }
        dialog.show()
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (wrong_credentials.isShown){
                wrong_credentials.visibility = View.GONE
            }
        }

        override fun afterTextChanged(s: Editable) {
            Log.e("info", "paso por aqui")
            when (view.id) {
                R.id.old_password -> validate_old_password()
                R.id.new_password -> validate_new_password()
                R.id.retype_new_password -> validate_retype_new_password()
            }

        }
    }

    private fun validate_old_password(): Boolean {

        if (old_password.text.toString().isEmpty()) {

            old_password_layout?.error = getString(R.string.empty_field)
            old_password_layout?.isErrorEnabled = true
            return false

        } else {
            old_password_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_new_password(): Boolean {

        if (new_password.text.toString().isEmpty()) {

            new_password_layout?.error = getString(R.string.empty_field)
            new_password_layout?.isErrorEnabled = true
            return false

        } else {
            new_password_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_retype_new_password(): Boolean {

        if (retype_new_password.text.toString().isEmpty()) {

            retype_new_password_layout?.error = getString(R.string.empty_field)
            retype_new_password_layout?.isErrorEnabled = true
            return false

        } else {
            retype_new_password_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun showServerErrors(errors: JSONArray) {
        old_password_layout?.isErrorEnabled = false
        new_password_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }



    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@ChangePassword_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@ChangePassword_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }


}
