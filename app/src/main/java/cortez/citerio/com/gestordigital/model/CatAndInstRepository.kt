package cortez.citerio.com.gestordigital.model

class CatAndInstRepository private constructor(private val catDao: CategoryDao, private val instDao: InstitutionDao){




    companion object {

        // For Singleton instantiation
        @Volatile private var instance: CatAndInstRepository? = null

        fun getInstance(catDao: CategoryDao, instDao: InstitutionDao) =
                instance ?: synchronized(this) {
                    instance ?: CatAndInstRepository(catDao, instDao).also { instance = it }
                }
    }
}