package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.app.NotificationManagerCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.util.LogoutListener
import cortez.citerio.com.gestordigital.util.NotificationDismissedReceiver
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.BaseApplication
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.tabs_screen.*
import org.json.JSONObject


class Tabs_Screen : BaseActivity() {
    val service = ServiceVolley()
    val apiController = APIController(service)
    private val TAG = "SistemaVirtualApp"
    private val FINISH_ACTIVITY = "FINISH"
    private var finishReceiver: ActivityBroadCastReceiver? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.transactionsFragment -> {
                loadTransactionsFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.servicesFragment -> {
                loadServicesFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.userFragment -> {
                loadUserFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tabs_screen)

        Log.v(TAG, "onCreate Tabs called")

        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Tabs_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@Tabs_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }else {

            /*navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

            if (savedInstanceState == null) {

                loadTransactionsFragment()
            }
            sendRegisterToken()*/
            val navController = Navigation.findNavController(this@Tabs_Screen, R.id.content)
            navigation.setupWithNavController(navController)

        }


        //finishReceiver = ActivityBroadCastReceiver()
    }

    fun loadTransactionsFragment() {

        val fragment = TransactionsFragment.newInstance("", "")
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content, fragment)
        ft.commit()
    }

    fun loadServicesFragment() {

        val fragment = ServicesFragment.newInstance("", "")
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content, fragment)
        ft.commit()
    }

    fun loadUserFragment() {

        val fragment = UserFragment.newInstance("", "")
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content, fragment)
        ft.commit()
    }

    fun sendRegisterToken() {
        val params = JSONObject()
        params.put("registertoken", Preference.getString(applicationContext, "registertoken"))
        apiController.post(EndPoints.URL_SEND_DEVICE_TOKEN, applicationContext, params,
            {response ->
                Log.i("envio", "exito")
            },
            {error ->
                Log.e("envio", "error")
            })
    }

    /*****************************receiving finish() command from base activity*****************************/
    private inner class ActivityBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                FINISH_ACTIVITY -> finish()
            }

        }
    }

    override fun onStart() {
        //applicationContext.registerReceiver(finishReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        super.onStart()
        Log.v(TAG, "onStart Tabs called")

    }

    override fun onResume() {
        super.onResume()
        Log.v(TAG, "onResume Tabs called")
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Tabs_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@Tabs_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }else{
            (application as BaseApplication).startUserSession()
            clearNotifications()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //applicationContext.unregisterReceiver(finishReceiver)
        Log.v(TAG, "onDestroy Tabs_Screen called")
    }



    /*****************************receiving finish() command from baseactivity*****************************/
}
