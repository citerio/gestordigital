package cortez.citerio.com.gestordigital

import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.*
import cortez.citerio.com.gestordigital.model.Category
import java.util.*


class CategoryAdapter(val items : ArrayList<Category>, val context: Context?, var res: Resources) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    // Gets the number of transactions in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.category_list_item, parent, false))
    }

    // Binds each transaction in the ArrayList to a view
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val categoryHolder: CategoryViewHolder? = holder as? CategoryViewHolder
        categoryHolder?.category_text_left?.text = items[position].name.take(1)
        categoryHolder?.category_text_right?.text = items[position].name
        categoryHolder?.itemView?.setOnClickListener{
            Log.e("capturo el click", "capturo el click")
            val services_screen = Intent(context, Services_Screen::class.java)
            services_screen.putExtra("categoryId", items[position].id)
            context?.startActivity(services_screen)
        }
    }

    class CategoryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val category_text_left = itemView.findViewById<TextView>(R.id.category_name        )
        val category_text_right = itemView.findViewById<TextView>(R.id.category_name)
        //val category_item = itemView.findViewById<LinearLayout>(R.id.category_item)
    }
}