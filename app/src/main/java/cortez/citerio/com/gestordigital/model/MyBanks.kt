package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MyBanks constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var bank_name: String = "",
        var bank_id: Long = 0,
        var user_account: String = "",
        var user_passport: String = "",
        var user_id: Long = 0
)
{
}