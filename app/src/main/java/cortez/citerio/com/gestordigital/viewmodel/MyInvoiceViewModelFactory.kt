package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cortez.citerio.com.gestordigital.model.MyInvoiceRepository

class MyInvoiceViewModelFactory (private val myInvoiceRepository: MyInvoiceRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyInvoiceViewModel(myInvoiceRepository) as T
    }

}