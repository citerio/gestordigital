package cortez.citerio.com.gestordigital.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import android.util.Log

interface ForegroundBackgroundListener : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startSomething() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopSomething() {}

}