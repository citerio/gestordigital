package cortez.citerio.com.gestordigital.volley

import android.content.Context
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import cortez.citerio.com.gestordigital.model.Preference
import org.json.JSONObject
import java.util.*

class ServiceVolley : ServiceInterface {
    val TAG = ServiceVolley::class.java.simpleName

    override fun getSimple(path: String, completionHandler: (response: JSONObject?) -> Unit) {

        val jsonObjReq = object : StringRequest(Method.GET, path,
                Response.Listener { response ->
                    Log.d("service get ok", "/get request OK! Response: $response")
                    val response = JSONObject(response)
                    completionHandler(response)
                },
                Response.ErrorListener { error ->
                    VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                    Log.e("service get error", "/get request Error! ${error.message}")
                    completionHandler(null)
                }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        BaseApplication.instance?.addToRequestQueue(jsonObjReq, TAG)
    }

    override fun get(path: String, context: Context, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {
        val jsonObjReq = object : StringRequest(Method.GET, path,
                Response.Listener { response ->
                    Log.d("service get ok", "/get request OK! Response: $response")
                    val response = JSONObject(response)
                    completionHandler(response)
                },
                Response.ErrorListener { error ->
                    Log.e("service get error", "/get request Error! ${error.message}")
                    errorHandler(error)
                }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                headers.put("Authorization", "Bearer ${Preference.getString(context, "usertoken")}")
                return headers
            }
        }

        BaseApplication.instance?.addToRequestQueue(jsonObjReq, TAG)
    }

    override fun postSimple(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {

        val jsonObjReq = object : JsonObjectRequest(Method.POST, path, params,
                Response.Listener<JSONObject> { response ->
                    Log.d("service post ok", "/post request OK! Response: $response")
                    completionHandler(response)
                },
                Response.ErrorListener { error ->
                    VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                    Log.e("service post error", "/post request Error! ${error.message}")
                    errorHandler(error)
                }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        BaseApplication.instance?.addToRequestQueue(jsonObjReq, TAG)
    }

    override fun post(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {
        val jsonObjReq = object : JsonObjectRequest(Method.POST, path, params,
                Response.Listener<JSONObject> { response ->
                    Log.d("service post ok", "/post request OK! Response: $response")
                    completionHandler(response)
                },
                Response.ErrorListener { error ->
                    VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                    Log.e("service post error", "/post request Error! ${error.message}")
                    errorHandler(error)
                }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                headers.put("Authorization", "Bearer ${Preference.getString(context, "usertoken")}")
                return headers
            }
        }

        BaseApplication.instance?.addToRequestQueue(jsonObjReq, TAG)
    }



    /*internal class PersistentCookieStore(val context: Context) : CookieStore {

        inner class DatedCookie(val cookie: HttpCookie, val creationDate: Date)

        val SP_COOKIE_STORE = "cookieStore"
        val SP_KEY_DELIMITER = "|"
        val SP_KEY_DELIMITER_REGEX = "\\" + SP_KEY_DELIMITER

        val cookieSharedPreferences = context.getSharedPreferences(SP_COOKIE_STORE, Context.MODE_PRIVATE)
        val allCookies: MutableMap<URI, MutableSet<DatedCookie>> = mutableMapOf()

        init {
            cookieSharedPreferences.all.entries.map {
                val uriAndName = it.key.split(Regex(SP_KEY_DELIMITER_REGEX), 2)
                val uri = URI(uriAndName[0])
                val cookie: DatedCookie = gson.fromJson<DatedCookie?>(it.value as String)!!
                if (allCookies[uri] == null) {
                    allCookies.put(uri, mutableSetOf())
                }
                allCookies[uri]?.add(cookie)
            }
        }

        fun getValidCookies(uri: URI) : List<HttpCookie>  {
            return allCookies.keys.filter {
                HttpCookie.domainMatches(it.host, uri.host)
                        //checkDomainsMatch(it.host, uri.host)
                        &&  checkPathsMatch(it.path, uri.path)
            } .flatMap {
                // clean expired cookies
                allCookies[it].orEmpty().filter { it.cookie.maxAge > 0 && Date().time > it.creationDate.time + (it.cookie.maxAge * 1000) }
                        .forEach {
                            remove(uri, it.cookie)
                        }
                allCookies[it].orEmpty().map { it.cookie }
            }
        }

        private fun checkDomainsMatch(cookieHost: String, requestHost: String): Boolean {
            return requestHost == cookieHost || requestHost.endsWith("." + cookieHost)
        }

        private fun checkPathsMatch(cookiePath: String, requestPath: String): Boolean {
            return requestPath == cookiePath ||
                    requestPath.startsWith(cookiePath) && cookiePath[cookiePath.length - 1] == '/' ||
                    requestPath.startsWith(cookiePath) && requestPath.substring(cookiePath.length)[0] == '/'
        }

        @Synchronized override fun add(uri: URI, cookie: HttpCookie) {
            var uri = uri

            uri = cookie.cookieUri(uri)
            var exists = allCookies[uri]
            if (exists == null)
                allCookies[uri] = HashSet<DatedCookie>()
            val datedCookie = DatedCookie(cookie, Date())
            allCookies[uri]?.add(datedCookie)
            saveToPersistence(uri, datedCookie)
        }

        private inline fun HttpCookie.cookieUri(uri: URI): URI {
            var cookieUri = uri
            if (domain != null) {
                // Remove the starting dot character of the domain, if exists (e.g: .domain.com -> domain.com)
                var domain = domain
                if (domain[0] == '.') {
                    domain = domain.substring(1)
                }
                try {
                    cookieUri = URI(if (uri.scheme == null)
                        "http"
                    else
                        uri.scheme, domain,
                            if (path == null) "/" else path, null)
                } catch (e: URISyntaxException) {
                    Log.w("ERROORRRR", e)
                }

            }
            return cookieUri
        }

        private fun saveToPersistence(uri: URI, cookie: DatedCookie) {
            val editor = cookieSharedPreferences.edit()
            editor.putString(uri.toString() + SP_KEY_DELIMITER + cookie.cookie.name, gson.toJson(cookie))
            editor.apply()
        }


        @Synchronized override fun remove(uri: URI, cookie: HttpCookie): Boolean {
            val targetCookies = allCookies[uri]
            val cookieRemoved = targetCookies != null && targetCookies.remove(targetCookies.find{ it.cookie == cookie })
            if (cookieRemoved) {
                removeFromPersistence(uri, cookie)
            }
            return cookieRemoved

        }

        private fun removeFromPersistence(uri: URI, cookieToRemove: HttpCookie) {
            val editor = cookieSharedPreferences.edit()
            editor.remove(uri.toString() + SP_KEY_DELIMITER
                    + cookieToRemove.name)
            editor.apply()
        }


        override fun removeAll(): Boolean {
            allCookies.clear()
            cookieSharedPreferences.edit().clear().apply()
            return true
        }

        override fun get(uri: URI?): MutableList<HttpCookie>? = getValidCookies(uri!!).toMutableList()
        override fun getURIs(): MutableList<URI>? = allCookies.keys.toMutableList()
        override fun getCookies(): MutableList<HttpCookie>? = allCookies.keys.flatMap { getValidCookies(it) }.toMutableList()
    }*/
}