package cortez.citerio.com.gestordigital.model

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import android.content.Context
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import cortez.citerio.com.gestordigital.util.SyncWorker

/**
 * Created by Administrador on 13/09/2017.
 */
@Database(entities = arrayOf(User::class, Provider::class, Bank::class, MyServices::class, MyBanks::class, MyTCs::class, MyInvoices::class, Institution::class, Category::class, MyInstitutions::class), version = 1)
//@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun providerDao(): ProviderDao
    abstract fun bankDao(): BankDao
    abstract fun myServicesDao(): MyServicesDao
    abstract fun myBanksDao(): MyBanksDao
    abstract fun myTCsDao(): MyTCsDao
    abstract fun myInvoicesDao(): MyInvoicesDao
    abstract fun institutionDao(): InstitutionDao
    abstract fun categoryDao(): CategoryDao
    abstract fun myInstitutionsDao(): MyInstitutionsDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "sistema_virtual")
                            .addCallback(object : RoomDatabase.Callback(){
                                override fun onCreate(db: SupportSQLiteDatabase) {
                                    super.onCreate(db)
                                    val request = OneTimeWorkRequest.Builder(SyncWorker::class.java).build()
                                    WorkManager.getInstance().enqueue(request)
                                }
                            })
                            //Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class)
                            //.addMigrations(MIGRATION_1_2)
                            .build()
                }

            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        /*internal val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                //database.execSQL("ALTER TABLE Activity" + " ADD COLUMN type INTEGER NOT NULL DEFAULT 1");
                //database.execSQL("ALTER TABLE Trip" + " ADD COLUMN map TEXT")
                database.execSQL("CREATE TABLE myinvoices as")
                //database.execSQL("ALTER TABLE Activity" + " ADD COLUMN text_stamp TEXT");
            }
        }*/
    }

}
