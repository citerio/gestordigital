package cortez.citerio.com.gestordigital

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.MyBanks
import cortez.citerio.com.gestordigital.model.MyTCs
import kotlinx.android.synthetic.main.banks_screen.*
import kotlinx.android.synthetic.main.tcs_screen.*
import kotlinx.android.synthetic.main.toolbar.*
import net.idik.lib.slimadapter.SlimAdapter

class TCs_Screen : AppCompatActivity() {

    /*****************************variables*****************************/

    private var serviceReceiver: ServiceBroadCastReceiver? = null
    private val NEW_SERVICE_ADDED = "NEW_SERVICE_ADDED"
    private val TAG = "SistemaVirtualApp"

    /*****************************variables*****************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tcs_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        serviceReceiver = ServiceBroadCastReceiver()

        add_tc_button.setOnClickListener{
            goToAddTC()
        }
    }

    override fun onResume() {
        super.onResume()
        show_tcs()
    }

    fun goToAddTC(){
        val affiliate_tc = Intent(this@TCs_Screen, New_TC_Screen::class.java)
        //affiliate_tc.putExtra("name", intent.getStringExtra("name"))
        affiliate_tc.putExtra("user_id", 0)
        startActivity(affiliate_tc)
    }

    /*****************************showing contacts from db*****************************/
    fun show_tcs(){

        object : AsyncTask<RecyclerView, Void, String>() {


            private var v: RecyclerView? = null
            private var message: String = ""
            private var tcs: ArrayList<MyTCs> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {
                    val database = AppDatabase.getDatabase(context = baseContext)
                    tcs = database?.myTCsDao()?.getTCsByUser(0) as ArrayList<MyTCs>
                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }


            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList(tcs)

                    if(!(tcs == null || tcs.size > 0)){
                        no_tcs.visibility = View.VISIBLE
                    }else{
                        no_tcs.visibility = View.INVISIBLE
                    }

                } else {

                    //v!!.visibility = View.GONE
                    no_tcs.visibility = View.VISIBLE
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, banks_list)


    }

    /*****************************receiving new contacts in db and updating contacts UI*****************************/
    private inner class ServiceBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                NEW_SERVICE_ADDED -> show_tcs()
            }

        }
    }

    /*****************************updating contacts UI*****************************/
    /*fun updateServices() {
        val contacts = contactQuery.find()
        adapter?.setContacts(contacts = contacts as ArrayList<Contact>)
        val items = adapter?.itemCount
        if(!(items == null || items <= 0)){
            no_contacts_?.visibility = View.INVISIBLE
        }
        Log.v(TAG, "update contacts called")
    }*/

    override fun onStart() {
        applicationContext.registerReceiver(serviceReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        super.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        applicationContext.unregisterReceiver(serviceReceiver)
    }

    fun refreshList(tcs: ArrayList<MyTCs>){

        Log.v(TAG, "refreshList called")

        adapter.updateData(tcs).notifyDataSetChanged()
    }

    private val adapter by lazy {
        SlimAdapter.create()
                .register<MyTCs>(R.layout.credit_card) { data, injector ->
                    injector
                            .text(R.id.cc_number, data.card_number)
                            .text(R.id.cc_due_date, data.card_due_date)
                            .clicked(R.id.see_button){
                                val intent = Intent(this@TCs_Screen, TC_See_Delete_Screen::class.java)
                                intent.putExtra("id", data.id)
                                startActivity(intent)
                            }

                }
                .attachTo(recyclerView)
    }


    private val recyclerView: RecyclerView by lazy<RecyclerView> {
        tcs_list.apply { // uxStores is the recyclerview’s name
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(this@TCs_Screen, LinearLayoutManager.VERTICAL))

        }!!
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.getItemId()) {
            R.id.add_button -> {
                goToAddTC()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
