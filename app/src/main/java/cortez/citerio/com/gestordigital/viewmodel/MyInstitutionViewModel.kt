package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import cortez.citerio.com.gestordigital.model.MyInstitutionRepository

class MyInstitutionViewModel internal constructor(private val myInstitutionRepository: MyInstitutionRepository) : ViewModel(){

    fun getMyInstitutionsByCategory(category: String) = myInstitutionRepository.getMyInstitutionsByCategory(category)
}