package cortez.citerio.com.gestordigital.model

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

data class InvoiceInst constructor(
        var id: String = "",
        var number: String = "",
        var amount: String = "",
        var date: String = "",
        var dueDate: String = "",
        var description: String = "",
        var status: Int = 0,
        var notification: Int = 0,
        var instName: String = "",
        var cateName: String = ""
){
}

@Dao
interface MyInvoicesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInvoice(invoice: MyInvoices)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInvoices(invoices: List<MyInvoices>)

    @Query("SELECT * FROM myinvoices WHERE id = :id")
    fun getInvoice(id: String): MyInvoices

    @Query("SELECT * FROM myinvoices")
    fun getAllInvoices(): List<MyInvoices>

    @Query("SELECT * FROM myinvoices WHERE status = :status")
    fun getInvoicesByStatus(status: Int): List<MyInvoices>

    //@Query("SELECT number, amount, strftime('%d/%m/%Y', `date`) as newdate, strftime('%d/%m/%Y', `dueDate`) as newduedate, description, status, category, name FROM myinvoices ORDER BY date DESC")
    //fun getAllInvoicesOrderByDate(): List<MyInvoices>

    //@Query("SELECT mi.`number`, mi.`amount`, substr(mi.`date`, 1, 10) as `newdate`, substr(mi.`duedate`, 1,10) as `newduedate`, mi.`description`, mi.`status`, mi.`category`, mi.`name` FROM myinvoices mi")
    //fun getAllInvoicesOrderByDate(): List<MyInvoices>

    //@Query("SELECT mi.`number`, mi.`amount`, mi.`date` as newdate, mi.`dueDate` as newduedate, mi.`description`, mi.`status`, mi.`category`, mi.`name` FROM myinvoices mi")
    //fun getAllInvoicesOrderByDate(): List<MyInvoices>

    //@Query("SELECT id, number, amount, substr(date, 1, 10) as date, substr(dueDate, 1, 10) as dueDate, description, status, category, name FROM myinvoices ORDER BY date DESC")
    //fun getAllInvoicesOrderByDate(): List<MyInvoices>

    //@Query("SELECT * FROM myinvoices ORDER BY date DESC")
    //fun getAllInvoicesOrderByDate(): List<MyInvoices>

    @Query("SELECT mi.id, mi.number, mi.amount, mi.date, mi.dueDate, mi.description, mi.status, mi.notification, ins.name AS instName, ca.name AS cateName FROM myinvoices mi JOIN institution ins ON mi.institution_id = ins.id JOIN category ca ON ins.category_id = ca.id ORDER BY date DESC")
    fun getAllInvoicesOrderByDate(): DataSource.Factory<Int, InvoiceInst>

    //@Query("SELECT * FROM myinvoices WHERE status = :status ORDER BY date DESC")
    //fun getInvoicesByStatusOrderByDate(status: Int): List<MyInvoices>

    @Query("SELECT mi.id, mi.number, mi.amount, mi.date, mi.dueDate, mi.description, mi.status, mi.notification, ins.name AS instName, ca.name AS cateName FROM myinvoices mi JOIN institution ins ON mi.institution_id = ins.id JOIN category ca ON ins.category_id = ca.id WHERE status = :status ORDER BY date DESC")
    fun getInvoicesByStatusOrderByDate(status: Int): DataSource.Factory<Int, InvoiceInst>

    @Query("SELECT COUNT (*) FROM myinvoices WHERE status = :status")
    fun getInvoicesByStatusCount(status: Int): LiveData<Int>

    @Query("DELETE FROM myinvoices")
    fun removeAllMyInvoices()

    @Query("UPDATE myinvoices SET status = :status WHERE id = :id")
    fun updateInvoicesStatus(id: String, status: Int): Int

    @Update
    fun updateInvoice(invoice: MyInvoices)

}