package cortez.citerio.com.gestordigital.model

class MyInstitutionRepository private constructor(private val myInstitutionsDao: MyInstitutionsDao){


    fun getMyInstitutionsByCategory(category: String) = myInstitutionsDao.getMyInstitutionsByCategory(category)

    fun addInstitution(institution: MyInstitutions){
        myInstitutionsDao.addInstitution(institution)
    }

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: MyInstitutionRepository? = null

        fun getInstance(myInstitutionsDao: MyInstitutionsDao) =
                instance ?: synchronized(this) {
                    instance ?: MyInstitutionRepository(myInstitutionsDao).also { instance = it }
                }
    }
}