package cortez.citerio.com.gestordigital

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import kotlinx.android.synthetic.main.change_secret_question_screen.*
import org.json.JSONObject
import android.util.Log
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import org.json.JSONArray

class ChangeSecretQuestion_Screen : BaseActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)
    private val FINISH_ACTIVITY = "FINISH"
    private var finishReceiver: ActivityBroadCastReceiver? = null

    val spinner1Array = ArrayList<String>()
    val spinner1Map = HashMap<Int, String>()
    val spinner2Array = ArrayList<String>()
    val spinner2Map = HashMap<Int, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_secret_question_screen)

        /*val adapter1 = ArrayAdapter.createFromResource(this@SecretQuestion_Screen,
                R.array.options_security_question_1, android.R.layout.simple_dropdown_item_1line)

        val adapter2 = ArrayAdapter.createFromResource(this@SecretQuestion_Screen,
                R.array.options_security_question_2, android.R.layout.simple_dropdown_item_1line)

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_ques_1.adapter = adapter1
        spinner_ques_2.adapter = adapter2*/

        answer1?.addTextChangedListener(MyTextWatcher(answer1))
        answer2?.addTextChangedListener(MyTextWatcher(answer2))

        val userId = intent.getStringExtra("userId")
        populateSpinners()

        // Set an on item selected listener for spinner object
        spinner_ques_1.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                //Toast.makeText(this@SecretQuestion_Screen, spinner1Map.get(spinner_ques_1.selectedItemPosition), Toast.LENGTH_SHORT).show();
                qst1_layout?.isErrorEnabled = false
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }

        // Set an on item selected listener for spinner object
        spinner_ques_2.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                //Toast.makeText(this@SecretQuestion_Screen, spinner2Map.get(spinner_ques_2.selectedItemPosition), Toast.LENGTH_SHORT).show();
                qst2_layout?.isErrorEnabled = false
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }

        update_button.setOnClickListener {
            progressbar.visibility = View.VISIBLE
            update_button.isEnabled = false
            cancel_button.isEnabled = false

            val qst1 = spinner_ques_1.selectedItemPosition
            val qst2 = spinner_ques_2.selectedItemPosition

            var valid = true
            if(qst1 == 0) {
                qst1_layout?.error = getString(R.string.empty_question)
                qst1_layout?.isErrorEnabled = true
                valid = false
            }
            if(TextUtils.isEmpty(answer1.text.toString())){
                answer1_layout?.error = getString(R.string.empty_field)
                answer1_layout?.isErrorEnabled = true
                valid = false
            }
            if(qst2 == 0) {
                qst2_layout?.error = getString(R.string.empty_question)
                qst2_layout?.isErrorEnabled = true
                valid = false
            }
            if(TextUtils.isEmpty(answer2.text.toString())){
                answer2_layout?.error = getString(R.string.empty_field)
                answer2_layout?.isErrorEnabled = true
                valid = false
            }

            if(valid) {
                val params = JSONObject()
                params.put("qst1", spinner1Map.get(spinner_ques_1.selectedItemPosition - 1)!!)
                params.put("qst2", spinner2Map.get(spinner_ques_1.selectedItemPosition - 1)!!)
                params.put("answer1", answer1.text.toString())
                params.put("answer2", answer2.text.toString())

                apiController?.post(EndPoints.URL_CHANGE_SQ, this@ChangeSecretQuestion_Screen, params,
                        { response ->
                            //progressbar.visibility = View.GONE
                            val result = response?.getString("result")
                            if (result == "OK") {
                                showDialogSuccess()
                            } else {
                                progressbar.visibility = View.GONE
                                update_button?.isEnabled = true
                                cancel_button?.isEnabled = true
                                Toast.makeText(this@ChangeSecretQuestion_Screen, "Ha ocurrido un error. Intente nuevamente.",
                                        Toast.LENGTH_SHORT).show();
                            }

                        },
                        { error ->
                            val networkResponse = error?.networkResponse
                            var jsonError: String = ""
                            var jsonObject: JSONObject? = null
                            if (networkResponse != null && networkResponse.data != null) {
                                jsonError = String(networkResponse.data)
                                jsonObject = JSONObject(jsonError)

                                if(jsonObject?.has("errors")) {
                                    showServerErrors(jsonObject?.getJSONArray("errors"))
                                } else {
                                    Toast.makeText(this@ChangeSecretQuestion_Screen, "Ha ocurrido un error. Intente nuevamente",
                                            Toast.LENGTH_SHORT).show();
                                }
                                // Print Error!
                            } else {
                                Toast.makeText(this@ChangeSecretQuestion_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            update_button?.isEnabled = true
                            cancel_button?.isEnabled = true
                            progressbar.visibility = View.GONE
                        })



                /*val que = Volley.newRequestQueue(this@ChangeSecretQuestion_Screen)
                val req = object: StringRequest(Request.Method.POST, EndPoints.URL_CHANGE_SQ,
                        Response.Listener {
                            response ->
                            showDialogSuccess()
                        }, Response.ErrorListener {
                    error ->
                    val networkResponse = error.networkResponse
                    var jsonError: String = ""
                    if (networkResponse != null && networkResponse.data != null) {
                        jsonError = String(networkResponse.data)
                        //showServerErrors(jsonError)
                        Log.i("response", jsonError)
                        // Print Error!
                    } else {
                        Toast.makeText(this@ChangeSecretQuestion_Screen, "Ha ocurrido un error. Intente nuevamente",
                                Toast.LENGTH_SHORT).show();
                    }
                    progressbar.visibility = View.GONE
                    update_button.isEnabled = true
                    cancel_button.isEnabled = true
                }) {
                    @Throws(AuthFailureError::class)
                    override fun getParams(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params.put("qst1", spinner1Map.get(spinner_ques_1.selectedItemPosition - 1)!!)
                        params.put("qst2", spinner2Map.get(spinner_ques_2.selectedItemPosition - 1)!!)
                        params.put("answer1", answer1.text.toString())
                        params.put("answer2", answer2.text.toString())
                        return params
                    }
                    override fun getHeaders(): Map<String, String> {
                        val headers = java.util.HashMap<String, String>()
                        headers.put("Content-Type", "application/json")
                        headers.put("Authorization", "Bearer ${Preference.getString(applicationContext, "usertoken")}")
                        return headers
                    }
                }

                que.add(req)*/
            } else {
                progressbar.visibility = View.GONE
                update_button.isEnabled = true
                cancel_button.isEnabled = true
            }

        }
        finishReceiver = ActivityBroadCastReceiver()
    }

    private fun populateSpinners() {

        apiController.getSimple(EndPoints.URL_GET_SQ) { response ->
            // Parse the result
            if(response != null) {
                val list1 = response?.getJSONArray("list1")
                val list2 = response?.getJSONArray("list2")

                spinner1Array.add(resources.getString(R.string.selec_ques))
                for (i in 0 until list1!!.length()) {
                    val json = list1.getJSONObject(i)
                    spinner1Array.add(json.getString("question"))
                    spinner1Map.put(i, json.getString("_id"))
                }

                spinner2Array.add(resources.getString(R.string.selec_ques))
                for (i in 0 until list2!!.length()) {
                    val json = list2.getJSONObject(i)
                    spinner2Array.add(json.getString("question"))
                    spinner2Map.put(i, json.getString("_id"))
                }

                val spinner_ques_1_adapter = ArrayAdapter<String>(this@ChangeSecretQuestion_Screen, R.layout.spinner_normal_item, spinner1Array)
                val spinner_ques_2_adapter = ArrayAdapter<String>(this@ChangeSecretQuestion_Screen, R.layout.spinner_normal_item, spinner2Array)
                spinner_ques_1_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
                spinner_ques_2_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
                spinner_ques_1.adapter = spinner_ques_1_adapter
                spinner_ques_2.adapter = spinner_ques_2_adapter

            } else {
                Log.e("errorleyendo", "errorleyendo")
            }
        }

    }

    private fun showDialogSuccess() {
        val dialogView = layoutInflater.inflate(R.layout.success_payment, null)
        val alert = AlertDialog.Builder(this@ChangeSecretQuestion_Screen)
        alert.setView(dialogView)
        val dialog = alert.create()
        dialog.setCancelable(false)

        val message = dialogView.findViewById<TextView>(R.id.message)

        message?.text = getString(R.string.success_sq_change)
        dialogView.findViewById<Button>(R.id.accept).setOnClickListener {
            dialog.dismiss()
            progressbar.visibility = View.GONE
            finish()
        }
        dialog.show()
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            when (view.id) {
                R.id.answer1 -> validate_answer1()
                R.id.answer2 -> validate_answer2()
            }

        }
    }

    private fun validate_answer1(): Boolean {

        if (answer1.text.toString().isEmpty()) {
            answer1_layout?.error = getString(R.string.empty_field)
            answer1_layout?.isErrorEnabled = true
            return false

        } else {
            answer1_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_answer2(): Boolean {

        if (answer2.text.toString().isEmpty()) {

            answer2_layout?.error = getString(R.string.empty_field)
            answer2_layout?.isErrorEnabled = true
            return false

        } else {
            answer2_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun showServerErrors(errors: JSONArray) {
        answer1_layout?.isErrorEnabled = false
        answer2_layout?.isErrorEnabled = false
        qst1_layout?.isErrorEnabled = false
        qst2_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }

    /*****************************receiving finish() command from base activity*****************************/
    private inner class ActivityBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                FINISH_ACTIVITY -> finish()
            }

        }
    }

    override fun onStart() {
        applicationContext.registerReceiver(finishReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        super.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        applicationContext.unregisterReceiver(finishReceiver)
    }
    /*****************************receiving finish() command from baseactivity*****************************/
}
