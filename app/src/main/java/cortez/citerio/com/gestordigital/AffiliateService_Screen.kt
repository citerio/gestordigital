package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.util.InjectorUtils
import cortez.citerio.com.gestordigital.viewmodel.InstitutionViewModel
import kotlinx.android.synthetic.main.affiliate_service_screen.*
import kotlinx.android.synthetic.main.toolbar.*
import net.idik.lib.slimadapter.SlimAdapter

class AffiliateService_Screen : BaseActivity() {

    /*****************************variables*****************************/

    private val TAG = "SistemaVirtualApp"
    private var imageResource: Int = 0
    private lateinit var viewModel: InstitutionViewModel

    /*****************************variables*****************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.affiliate_service_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        val categoryId = intent.getStringExtra("categoryId")

        /*when(type){
            1 ->{
                provider_title.text = resources.getString(R.string.school)
                provider_logo.setImageResource(R.mipmap.baseline_school_black_36)
                imageResource = R.mipmap.baseline_school_black_36
            }
            2 ->{
                provider_title.text = resources.getString(R.string.university)
                provider_logo.setImageResource(R.mipmap.baseline_account_balance_black_36)
                imageResource = R.mipmap.baseline_account_balance_black_36
            }
            3 ->{
                provider_title.text = resources.getString(R.string.gym)
                provider_logo.setImageResource(R.mipmap.baseline_fitness_center_black_36)
                imageResource = R.mipmap.baseline_fitness_center_black_36
            }
            4 ->{
                provider_title.text = resources.getString(R.string.electricity)
                provider_logo.setImageResource(R.mipmap.baseline_highlight_black_36)
                imageResource = R.mipmap.baseline_highlight_black_36
            }
            5 ->{
                provider_title.text = resources.getString(R.string.water)
                provider_logo.setImageResource(R.mipmap.baseline_local_drink_black_36)
                imageResource = R.mipmap.baseline_local_drink_black_36
            }
            6 ->{
                provider_title.text = resources.getString(R.string.cable_tv)
                provider_logo.setImageResource(R.mipmap.baseline_live_tv_black_36)
                imageResource = R.mipmap.baseline_live_tv_black_36
            }
        }*/

        val factory = InjectorUtils.provideInstitutionViewModelFactory(this@AffiliateService_Screen)
        viewModel = ViewModelProviders.of(this, factory).get(InstitutionViewModel::class.java)

        show_providers()
    }

    /*****************************showing contacts from db*****************************/
    /*fun show_providers(){

        object : AsyncTask<RecyclerView, Void, String>() {


            private var v: RecyclerView? = null
            private var message: String = ""
            private var providers: ArrayList<Institution> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    providers = database?.institutionDao()?.getInstitutionsByCategory(intent.getStringExtra("categoryId") ) as ArrayList<Institution>
                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }

            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList(providers)

                } /*else {

                    //v!!.visibility = View.GONE
                    no_services.visibility = View.VISIBLE
                }*/

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, providers_list)


    }*/

    fun show_providers(){
        viewModel.getInstitutionsByCategory(intent.getStringExtra("categoryId")).observe(this@AffiliateService_Screen, Observer { inst ->
            if (inst != null){
                adapter.updateData(inst).notifyDataSetChanged()
            }
        })
    }

    fun refreshList(providers: ArrayList<Institution>){

        Log.v(TAG, "refreshList called")

        adapter.updateData(providers).notifyDataSetChanged()
    }

    private val adapter by lazy {
        SlimAdapter.create()
                .register<Institution>(R.layout.provider) { data, injector ->
                    injector
                            .text(R.id.provider_name, data.name)
                            .text(R.id.provider_fl, data.name.first().toUpperCase().toString())
                            /*.with(R.id.provider_logo, IViewInjector.Action<ImageView>{
                                it.setImageResource(imageResource)
                            })*/
                            .clicked(R.id.cv) {
                                val new_service_screen = Intent(this@AffiliateService_Screen, New_Service_Screen::class.java)
                                //new_service_screen.putExtra("type", data.type)
                                //new_service_screen.putExtra("name", data.name)
                                new_service_screen.putExtra("institution_id", data.id)
                                startActivity(new_service_screen)
                            }

                }
                .attachTo(recyclerView)
    }


    private val recyclerView: RecyclerView by lazy<RecyclerView> {
        providers_list.apply { // uxStores is the recyclerview’s name
            layoutManager = LinearLayoutManager(baseContext, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(this@AffiliateService_Screen, LinearLayoutManager.VERTICAL))

        }!!
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@AffiliateService_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@AffiliateService_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }

}
