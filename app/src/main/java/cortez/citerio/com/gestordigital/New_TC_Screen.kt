package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.new_tc_screen.*
import android.text.TextUtils
import android.widget.Button
import android.widget.TextView
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class New_TC_Screen : BaseActivity() {

    private val TAG = "SistemaVirtualApp"
    private var isDelete = false;
    val service = ServiceVolley()
    val apiController = APIController(service)
    private val FINISH_ACTIVITY = "FINISH"
    private var finishReceiver: ActivityBroadCastReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_tc_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        cardNumber.addTextChangedListener(MyTextWatcher(cardNumber))
        cvc.addTextChangedListener(MyTextWatcher(cvc))
        expirationDate.addTextChangedListener(MyTextWatcher(expirationDate))
        cardHolderID.addTextChangedListener(MyTextWatcher(cardHolderID))
        cardHolder.addTextChangedListener(MyTextWatcher(cardHolder))

        affiliate_button.setOnClickListener{
            affiliate_button.isEnabled = false
            cancel_button.isEnabled = false
            addTCtoServer()
        }

        cancel_button.setOnClickListener {
            finish()
        }

        finishReceiver = ActivityBroadCastReceiver()
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {




        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            isDelete = before != 0

        }

        override fun afterTextChanged(s: Editable) {

            when (view.id) {
                R.id.cardNumber -> validate_cardNumber()
                R.id.expirationDate -> validate_expirationDate()
                R.id.cvc -> validate_cvc()
                R.id.cardHolder -> validate_name()
                R.id.cardHolderID -> validate_id()
            }

        }
    }

    private fun validate_cardNumber(): Boolean {

        if (cardNumber.text.toString().isEmpty()) {

            cardNumber_layout.error = getString(R.string.empty_field)

            return false

        } else {

            if (cardNumber.text.toString().length < 18){
                cardNumber_layout.error = resources.getString(R.string.invalid_card_length)
            }else{
                cardNumber_layout.isErrorEnabled = false
            }

            val source = cardNumber.text.toString()
            val length = source.length
            val stringBuilder = StringBuilder()
            stringBuilder.append(source)
            if (length > 0 && length%5==0){
                if (isDelete){
                    stringBuilder.deleteCharAt(length - 1)
                }else{
                    stringBuilder.insert(length - 1, " ")
                }
                cardNumber.setText(stringBuilder)
                cardNumber.setSelection(cardNumber.text.length)

            }

        }

        return true

    }

    private fun validate_expirationDate(): Boolean {

        if (expirationDate.text.toString().isEmpty()) {

            expirationDate_layout.error = getString(R.string.empty_field)

            return false

        } else {

            expirationDate_layout.isErrorEnabled = false
            val source = expirationDate.text.toString()
            val length = source.length
            val stringBuilder = StringBuilder()
            stringBuilder.append(source)
            if (length > 0 && length==3){
                if (isDelete){
                    stringBuilder.deleteCharAt(length - 1)
                }else{
                    stringBuilder.insert(length - 1, "/")
                }
                expirationDate.setText(stringBuilder)
                expirationDate.setSelection(expirationDate.text.length)
            }
            if (!isValidDate(stringBuilder.toString())){
                expirationDate_layout.error = getString(R.string.invalid_due_date)
                return false
            }

        }

        return true

    }

    private fun validate_name(): Boolean {

        if (cardHolder.text.toString().isEmpty()) {

            cardHolder_layout?.error = getString(R.string.empty_field)
            cardHolder_layout?.isErrorEnabled = true
            return false

        } else {
            cardHolder_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_id(): Boolean {

        when {
            cardHolderID.text.toString().isEmpty() -> {

                cardHolderID_layout?.error = getString(R.string.empty_field)
                cardHolderID_layout?.isErrorEnabled = true
                return false

            }
            cardHolderID.text.toString().length < 6 -> {

                cardHolderID_layout?.error = getString(R.string.invalid_user_id_length)
                cardHolderID_layout?.isErrorEnabled = true
                return false

            }
            else -> cardHolderID_layout?.isErrorEnabled = false
        }

        return true

    }


    private fun validate_cvc(): Boolean {

        if (cvc.text.toString().isEmpty()) {

            cvc_layout.error = getString(R.string.empty_field)

            return false

        } else {

            cvc_layout.isErrorEnabled = false

        }

        return true

    }

    fun isValidDate(cardValidity: String): Boolean {
        if (!TextUtils.isEmpty(cardValidity) && cardValidity.length == 7) {
            val month = cardValidity.substring(0, 2)
            val year = cardValidity.substring(3, 7)

            var monthpart = -1
            var yearpart = -1

            try {
                monthpart = Integer.parseInt(month) - 1
                yearpart = Integer.parseInt(year)

                val current = Calendar.getInstance()
                current.set(Calendar.DATE, 1)
                current.set(Calendar.HOUR, 12)
                current.set(Calendar.MINUTE, 0)
                current.set(Calendar.SECOND, 0)
                current.set(Calendar.MILLISECOND, 0)

                val validity = Calendar.getInstance()
                validity.set(Calendar.DATE, 1)
                validity.set(Calendar.HOUR, 12)
                validity.set(Calendar.MINUTE, 0)
                validity.set(Calendar.SECOND, 0)
                validity.set(Calendar.MILLISECOND, 0)

                if (monthpart > -1 && monthpart < 12 && yearpart > -1) {
                    validity.set(Calendar.MONTH, monthpart)
                    validity.set(Calendar.YEAR, yearpart)
                } else
                    return false

                Log.v("Util", "isValidDate: " + current.compareTo(validity))

                return current.compareTo(validity) < 0
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        return false
    }

    /*****************************adding a new service to DB*****************************/
    fun addTCtoServer(){

        var valid = true

        if (TextUtils.isEmpty(cardNumber.text.toString().trim())) {
            cardNumber_layout?.error = getString(R.string.empty_field)
            cardNumber_layout?.isErrorEnabled = true
            valid = false
        }
        if (TextUtils.isEmpty(expirationDate.text.toString().trim())) {
            expirationDate_layout?.error = getString(R.string.empty_field)
            expirationDate_layout?.isErrorEnabled = true
            valid = false
        }
        if (TextUtils.isEmpty(cvc.text.toString().trim())) {
            cvc_layout?.error = getString(R.string.empty_field)
            cvc_layout?.isErrorEnabled = true
            valid = false
        }
        if (TextUtils.isEmpty(cardHolder.text.toString().trim())) {
            cardHolder_layout?.error = getString(R.string.empty_field)
            cardHolder_layout?.isErrorEnabled = true
            valid = false
        }
        if (TextUtils.isEmpty(cardHolderID.text.toString().trim())) {
            cardHolderID_layout?.error = getString(R.string.empty_field)
            cardHolderID_layout?.isErrorEnabled = true
            valid = false
        }

        if (cardNumber_layout.isErrorEnabled || expirationDate_layout.isErrorEnabled || cvc_layout.isErrorEnabled || cardHolder_layout.isErrorEnabled || cardHolderID_layout.isErrorEnabled){
            valid = false
        }

        if (valid){

            progressbar.visibility = View.VISIBLE

            val params = JSONObject()
            params.put("cardNumber", cardNumber.text.toString().trim() )
            //params.put("cardNumber", "4111111111111111" )
            params.put("expirationDate", expirationDate.text.toString())
            params.put("cvc", cvc.text.toString().trim())
            params.put("cardHolder", cardHolder.text.toString().trim())
            params.put("cardHolderID", cardHolderID.text.toString().trim())
            params.put("invoiceId", intent.getStringExtra("invoiceId"))

            apiController.post(EndPoints.URL_PAY_INVOICE, this@New_TC_Screen, params,
                    {response ->

                        val result = response?.getString("result")
                        if (result == "OK") {
                            Log.e("bien", "todo bien")
                            updateInvoiceStatus(response?.getString("htmlReceipt"))
                            Log.v("voucher", response?.getString("htmlReceipt"))
                        } else {
                            progressbar.visibility = View.GONE
                            affiliate_button?.isEnabled = true
                            cancel_button?.isEnabled = true
                            Toast.makeText(this@New_TC_Screen, "Lo sentimos ha ocurrido un error. Intentelo nuevamente.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    },
                    {error ->
                        val networkResponse = error?.networkResponse
                        var jsonError: String = ""
                        var jsonObject: JSONObject? = null
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = String(networkResponse.data)
                            jsonObject = JSONObject(jsonError)

                            if(jsonObject?.has("errors")) {
                                showServerErrors(jsonObject?.getJSONArray("errors"))
                            } else {
                                if(jsonObject?.has("message")) {
                                    Toast.makeText(this@New_TC_Screen, jsonObject.getString("message"),
                                        Toast.LENGTH_SHORT).show();
                                } else {
                                    Log.e("error1", "error1")
                                    Toast.makeText(this@New_TC_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                                }
                            }
                            // Print Error!
                        } else {
                            Log.e("error2", "error2")
                            Toast.makeText(this@New_TC_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        affiliate_button?.isEnabled = true
                        cancel_button.isEnabled = true
                        progressbar.visibility = View.GONE
                    }
            )


        } else {
            affiliate_button?.isEnabled = true
            cancel_button.isEnabled = true
            progressbar.visibility = View.GONE
        }


    }

    /*****************************adding a new service to DB*****************************/
    /*fun addTCtoDB(){

        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val tc  = MyTCs( cardNumber = cardNumber.text.toString(), expirationDate = expirationDate.text.toString(), cvc = cvc.text.toString(), cardHolderID = 0, cardHolder = "" )
                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myTCsDao()?.addTC(tc)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                progressbar.visibility = View.GONE

                if (m.equals("success")) {


                    try {

                        Log.v(TAG, "New tc inserted in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        Toast.makeText(this@New_TC_Screen, "Nueva tarjeta de credito agregada", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting new cc in DB")
                    Toast.makeText(this@New_TC_Screen, "ERROR inside adding cc", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }*/

    private fun updateInvoiceStatus(htmlReceipt: String) {
        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myInvoicesDao()?.updateInvoicesStatus(intent.getStringExtra("invoiceId"), 1)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {
                    try {
                        Log.v(TAG, "Invoice updated")
                        val intentReceipt = Intent(this@New_TC_Screen, Receipt_Screen::class.java)
                        intentReceipt.putExtra("htmlReceipt", htmlReceipt)
                        startActivity(intentReceipt)
                        finish()
                        //showDialogSuccess()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on updating invoice")
                    Toast.makeText(this@New_TC_Screen, "Error realizando el pago.", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")
    }

    private fun showDialogSuccess() {
        progressbar.visibility = View.GONE
        val dialogView = layoutInflater.inflate(R.layout.success_payment, null)
        val alert = AlertDialog.Builder(this@New_TC_Screen)
        alert.setView(dialogView)
        val dialog = alert.create()
        dialog.setCancelable(false)

        val message = dialogView.findViewById<TextView>(R.id.message)

        message?.text = getString(R.string.up_to_date) + " " + intent.getStringExtra("name")
        dialogView.findViewById<Button>(R.id.accept).setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@New_TC_Screen, Tabs_Screen::class.java)
            startActivity(intent)
            finish()
        }
        dialog.show()
    }

    private fun showServerErrors(errors: JSONArray) {
        cardNumber_layout?.isErrorEnabled = false
        expirationDate_layout?.isErrorEnabled = false
        cvc_layout?.isErrorEnabled = false
        cardHolder_layout?.isErrorEnabled = false
        cardHolderID_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }

    /*****************************receiving finish() command from base activity*****************************/
    private inner class ActivityBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                FINISH_ACTIVITY -> finish()
            }

        }
    }

    override fun onStart() {
        applicationContext.registerReceiver(finishReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@New_TC_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@New_TC_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }

    override fun onDestroy() {
        applicationContext.unregisterReceiver(finishReceiver)
        super.onDestroy()
    }
    /*****************************receiving finish() command from baseactivity*****************************/
}
