package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Bundle
import androidx.core.app.NotificationManagerCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.google.gson.Gson
import cortez.citerio.com.gestordigital.model.Notification
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.util.LogoutListener
import cortez.citerio.com.gestordigital.util.NotificationDismissedReceiver
import cortez.citerio.com.gestordigital.volley.BaseApplication



abstract class BaseActivity : AppCompatActivity() {

    private val TAG = "SistemaVirtualApp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.v(TAG, "onCreate BaseActivity called")

        (application as BaseApplication).registerSessionListener(object : LogoutListener {
            override fun onSessionLogout() {
                Log.v(TAG, "onSessionLogout called")
                Preference.saveString(applicationContext, "usertoken", "")
                /*val intent_ma = Intent("cortez.citerio.com.gestordigital")
                intent_ma.putExtra("RESULT_CODE", "FINISH")
                applicationContext.sendBroadcast(intent_ma)*/
                //finish()
                //val sign_in_screen = Intent(this@BaseActivity, SignIn_Screen::class.java)
                //startActivity(sign_in_screen)
                if (Preference.getBoolean(applicationContext, "appON")){
                    val intentToMain = Intent(this@BaseActivity, SignIn_Screen::class.java)
                    val sBuilder = TaskStackBuilder.create(this@BaseActivity)
                    sBuilder.addNextIntentWithParentStack(intentToMain)
                    sBuilder.startActivities()
                }

            }
        })

    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        Log.v(TAG, "onUserInteraction called")
        if(Preference.getString(applicationContext, "usertoken").isNotEmpty()){
            (application as BaseApplication).onUserInteracted()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v(TAG, "onDestroy BaseActivity called")
    }

    fun clearNotifications(){

        NotificationManagerCompat.from(this).apply {
            cancelAll()
        }

        val intentDismissed = Intent(this, NotificationDismissedReceiver::class.java)
        sendBroadcast(intentDismissed)
    }




}