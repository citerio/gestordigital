package cortez.citerio.com.gestordigital

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.logging_system_screen.*
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.FirebaseAuth
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import org.json.JSONArray


class Logging_System_Screen : AppCompatActivity() {

    // [START declare_auth]
    private var mAuth: FirebaseAuth? = null
    private val TAG = "SistemaVirtualApp"
    val service = ServiceVolley()
    val apiController = APIController(service)
    var database: AppDatabase? = null
    val REQUEST_EXIT = 567

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logging_system_screen)

        mAuth = FirebaseAuth.getInstance();

        ///sign in button
        signin_button.setOnClickListener {
            val sign_in_screen = Intent(this@Logging_System_Screen, SignIn_Screen::class.java)
            startActivityForResult(sign_in_screen, REQUEST_EXIT)
        }
        ///create account button
        create_account_button.setOnClickListener{
            val create_account_screen = Intent(this@Logging_System_Screen, CreateAccount_Screen::class.java)
            startActivity(create_account_screen)
        }
    }

    public override fun onStart() {
        super.onStart()
        //database = AppDatabase.getDatabase(context = baseContext)
        //addProviders()
        getCategoriesAndInstitutions()
        if(Preference.getString(applicationContext, "usertoken") != null && Preference.getString(applicationContext, "usertoken") != ""){
            val intentToMain = Intent(this@Logging_System_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }
    }

    fun getCategoriesAndInstitutions() {
        Log.e("isUpdated", Preference.getBoolean(applicationContext, "isUpdated").toString())
        if(Preference.getBoolean(applicationContext, "isUpdated") == null || !Preference.getBoolean(applicationContext, "isUpdated")){
            apiController.get(EndPoints.URL_GET_INST_AND_CATE, applicationContext,
                {response ->
                    val categories = response?.getJSONArray("categories")
                    val institutions = response?.getJSONArray("institutions")
                    Log.i("categories", "$categories")
                    Log.i("institutions", "$institutions")
                    addCategoriesAndInstitutions(categories!!, institutions!!)
                },
                {_ ->
                    Toast.makeText(applicationContext, "Ha ocurrido un error obteniendo las categorias. Intente nuevamente",
                            Toast.LENGTH_SHORT).show();
                })
        } else {
            Log.e("nonull", "nonull")
        }
    }

    fun addCategoriesAndInstitutions(categories: JSONArray, institutions: JSONArray) {
        object : AsyncTask<String, Void, String>() {

            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {
                    var cates : ArrayList<Category> = ArrayList<Category>()
                    var insts : ArrayList<Institution> = ArrayList<Institution>()

                    for (i in 0 until categories!!.length()) {
                        val id = categories.getJSONObject(i).getString("_id")
                        val name = categories.getJSONObject(i).getString("name")

                        val cate = Category(id = id, name = name)
                        cates.add(cate)
                    }

                    for (i in 0 until institutions!!.length()) {
                        val id = institutions.getJSONObject(i).getString("_id")
                        val name = institutions.getJSONObject(i).getString("name")
                        val category_id = institutions.getJSONObject(i).getString("institutionCategoryId")

                        val inst = Institution(id = id, name = name, category_id = category_id)
                        insts.add(inst)
                    }

                    val database = AppDatabase.getDatabase(context = baseContext)
                    //database?.categoryDao()?.removeAllCategories()
                    //database?.institutionDao()?.removeAllInstitutions()
                    database?.categoryDao()?.addCategories(categories = cates)
                    database?.institutionDao()?.addInstitutions(institutions = insts)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {
                        Preference.saveBoolean(applicationContext, "isUpdated", true)
                        Log.v(TAG, "categories inserted in DB")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting categories in DB")
                    Toast.makeText(this@Logging_System_Screen, "ERROR inside adding categories", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")
    }

    /*****************************adding products to DB*****************************/
    fun addProviders(){
        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    var providers : ArrayList<Provider> = ArrayList()
                    var banks : ArrayList<Bank> = ArrayList()
                    //1 escuela
                    //2 universidad
                    //3 gym
                    //4 electricidad
                    //5 agua
                    //6 tv por cable
                    providers.add(Provider(id = 1, name = "Colegio de Ingenieros de Barquisimeto", type = 1))
                    /*providers.add(Provider(id = 2,name = "Escuela Aplicacion", type = 1))
                    providers.add(Provider(id = 3,name = "Colegio Sagrado Corazon", type = 1))
                    providers.add(Provider(id = 4,name = "Colegio Santa Rosa", type = 1))
                    providers.add(Provider(id = 5,name = "Colegio Don Bosco", type = 1))*/
                    providers.add(Provider(id = 6,name = "Club Luso Larense", type = 2))
                    /*providers.add(Provider(id = 7,name = "Universidad Yacambu", type = 2))
                    providers.add(Provider(id = 8,name = "Universidad de Carabobo", type = 2))
                    providers.add(Provider(id = 9,name = "Universidad Central de Venezuela", type = 2))*/
                    providers.add(Provider(id = 10,name = "Gala Gym", type = 3))
                    /*providers.add(Provider(id = 11,name = "Gold's Gym", type = 3))
                    providers.add(Provider(id = 12,name = "Francis Power House Gym", type = 3))
                    providers.add(Provider(id = 13,name = "Planet Fitness", type = 3))*/
                    providers.add(Provider(id = 14,name = "Corpoelec", type = 4))
                    //providers.add(Provider(id = 15,name = "Hidrocapital", type = 5))
                    providers.add(Provider(id = 15,name = "Ciudad Roca", type = 5))
                    providers.add(Provider(id = 16,name = "El Pedregal", type = 5))
                    /*providers.add(Provider(id = 17,name = "Hidrolara", type = 5))
                    providers.add(Provider(id = 18,name = "Hidrozulia", type = 5))*/
                    providers.add(Provider(id = 19,name = "Directv", type = 6))
                    providers.add(Provider(id = 20,name = "Inter", type = 6))
                    providers.add(Provider(id = 21,name = "Supercable", type = 6))

                    banks.add(Bank(id = 1, name = "Bancolombia"))
                    banks.add(Bank(id = 2, name = "Banco Provincial"))
                    banks.add(Bank(id = 3, name = "Banesco"))

                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myServicesDao()?.removeAllMyServices()
                    database?.myBanksDao()?.removeAllMyBanks()
                    database?.myTCsDao()?.removeAllMyTCs()
                    database?.providerDao()?.addProviders(provider = providers)
                    database?.bankDao()?.addBanks(bank = banks)

                    message = "success";
                    return message;



                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        Log.v(TAG, "Providers inserted in DB")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting providers in DB")
                    Toast.makeText(this@Logging_System_Screen, "ERROR inside adding providers", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }
    /*****************************adding products to DB*****************************/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }


}
