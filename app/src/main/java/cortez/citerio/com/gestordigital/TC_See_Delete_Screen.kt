package cortez.citerio.com.gestordigital

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.widget.Toast
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.MyTCs
import kotlinx.android.synthetic.main.tc_see_delete_screen.*
import kotlinx.android.synthetic.main.toolbar.*


class TC_See_Delete_Screen : AppCompatActivity() {

    private val TAG = "SistemaVirtualApp"
    private var isDelete = false
    private lateinit var tc : MyTCs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tc_see_delete_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        card_number.keyListener = null
        card_due_date.keyListener = null
        card_cvc.keyListener = null

        delete_button.setOnClickListener{
            confirmDeleteTC()
        }

        ok_button.setOnClickListener {
            finish()
        }

        getTC()
    }



    /*****************************adding a new service to DB*****************************/
    fun getTC(){


        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    tc = database?.myTCsDao()?.getTC(intent.getLongExtra("id", -1))!!

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        if (tc != null){
                            card_number.setText(tc.card_number)
                            card_due_date.setText(tc.card_due_date)
                            card_cvc.setText(tc.card_cvc)
                        }else{
                            Log.v(TAG, "TC not found")
                        }

                        Log.v(TAG, "New tc queried in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        //Toast.makeText(this@Service_See_Delete_Screen, "Nuevo comercio agregado", Toast.LENGTH_LONG).show()
                        //finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on querying tc in DB")
                    //Toast.makeText(this@Service_See_Delete_Screen, "ERROR inside adding service", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }

    fun deleteTC(){


        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myTCsDao()?.deleteTC(tc)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        Log.v(TAG, "New tc queried in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        Toast.makeText(this@TC_See_Delete_Screen, "Tarjeta de Credito Eliminada", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on deleting tc in DB")
                    //Toast.makeText(this@Service_See_Delete_Screen, "ERROR inside adding service", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }

    fun confirmDeleteTC(){

        AlertDialog.Builder(this@TC_See_Delete_Screen)
                .setTitle("Confirme")
                .setMessage("¿Esta seguro de eliminar esta tarjeta de credito?")
                .setPositiveButton(resources.getString(R.string.ok)) { _, _ ->
                    deleteTC()
                }
                .setNegativeButton("Cancel") { _, _ ->

                }.show()

    }
}
