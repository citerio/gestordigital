package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.new_service_screen.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject

class New_Service_Screen : BaseActivity() {

    private val TAG = "SistemaVirtualApp"
    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_service_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        user_id.addTextChangedListener(MyTextWatcher(user_id))
        //user_contract.addTextChangedListener(MyTextWatcher(user_contract))

        affiliate_button.setOnClickListener{


                if(!user_id.text.toString().trim().equals("") /*&& !user_contract.text.toString().trim().equals("")*/ ){
                    sendServiceToServer()
                    //addService()

                }else{
                    validate_name()
                    //Toast.makeText(this@New_Service_Screen, resources.getString(R.string.empty_field), Toast.LENGTH_LONG).show()
                }
        }




        cancel_button.setOnClickListener {
            finish()
        }


    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {

            when (view.id) {
                R.id.user_id -> validate_name()
                //R.id.user_contract -> validate_contract()
            }

        }
    }

    private fun validate_name(): Boolean {

        if (user_id.text.toString().isEmpty()) {

            user_id_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_id_layout.isErrorEnabled = false

        }

        return true

    }

    /*private fun validate_contract(): Boolean {

        if (user_contract.text.toString().isEmpty()) {

            user_contract_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_contract_layout.isErrorEnabled = false

        }

        return true

    }*/

    fun sendServiceToServer() {
        val params = JSONObject()
        params.put("user_id", user_id?.text.toString())
        params.put("institution_id", intent.getStringExtra("institution_id"))
        affiliate_button.isEnabled = false
        progressbar.visibility = View.VISIBLE

        apiController.post(EndPoints.URL_REGISTER_SERVICE, this@New_Service_Screen, params,
                { response ->
                    val result = response?.getString("result")
                    if (result == "OK") {
                        addService()
                        /*val intent = Intent(this@New_Service_Screen, SignIn_Screen::class.java)
                        startActivity(intent)
                        finish()*/
                    } else {
                        progressbar.visibility = View.GONE
                        affiliate_button.isEnabled = true
                        Toast.makeText(this@New_Service_Screen, "Ha ocurrido un error. Intente nuevamente",
                                Toast.LENGTH_SHORT).show();
                    }
                },
                { error ->
                    val networkResponse = error?.networkResponse
                    var jsonError: String = ""
                    var jsonObject: JSONObject? = null
                    if (networkResponse != null && networkResponse.data != null) {
                        jsonError = String(networkResponse.data)
                        jsonObject = JSONObject(jsonError)

                        if(jsonObject?.has("errors")) {
                            showServerErrors(jsonObject?.getJSONArray("errors"))
                        } else {
                            Toast.makeText(this@New_Service_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // Print Error!
                    } else {
                        Toast.makeText(this@New_Service_Screen, "Ha ocurrido un error. Intente nuevamente",
                                Toast.LENGTH_SHORT).show();
                    }
                    affiliate_button.isEnabled = true
                    progressbar.visibility = View.GONE
                })
    }

    /*****************************adding a new service to DB*****************************/
    fun addService(){


        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val service  = MyInstitutions( institution_id = intent.getStringExtra("institution_id"), user_passport = user_id.text.toString())
                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myInstitutionsDao()?.addInstitution(service)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        Log.v(TAG, "New service inserted in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        Toast.makeText(this@New_Service_Screen, "Nuevo servicio agregado", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting new service in DB")
                    Toast.makeText(this@New_Service_Screen, "ERROR inside adding service", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@New_Service_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@New_Service_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }


    private fun showServerErrors(errors: JSONArray) {
        user_id_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }

    /*****************************receiving finish() command from baseactivity*****************************/
}
