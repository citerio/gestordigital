package cortez.citerio.com.gestordigital.volley

import android.content.Context
import com.android.volley.VolleyError
import org.json.JSONObject

interface ServiceInterface {
    fun get(path: String, context: Context, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit)
    fun getSimple(path: String, completionHandler: (response: JSONObject?) -> Unit)
    fun post(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit)
    fun postSimple(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit)
}