package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cortez.citerio.com.gestordigital.model.CategoryRepository

class CategoryViewModelFactory (private val categoryRepository: CategoryRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryViewModel(categoryRepository) as T
    }

}