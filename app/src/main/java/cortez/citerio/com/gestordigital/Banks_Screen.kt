package cortez.citerio.com.gestordigital

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.MyBanks
import kotlinx.android.synthetic.main.banks_screen.*
import net.idik.lib.slimadapter.SlimAdapter

class Banks_Screen : AppCompatActivity() {

    /*****************************variables*****************************/

    private var serviceReceiver: ServiceBroadCastReceiver? = null
    private val NEW_SERVICE_ADDED = "NEW_SERVICE_ADDED"
    private val TAG = "SistemaVirtualApp"

    /*****************************variables*****************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.banks_screen)

        setSupportActionBar(toolbar_banks)
        supportActionBar?.title = ""
        serviceReceiver = ServiceBroadCastReceiver()

        add_bank_button.setOnClickListener{
            goToAddBank()
        }
    }

    override fun onResume() {
        super.onResume()
        show_bank_accounts()
    }

    fun goToAddBank(){
        val affiliate_service = Intent(this@Banks_Screen, New_Bank_Screen::class.java)
        affiliate_service.putExtra("name", intent.getStringExtra("name"))
        affiliate_service.putExtra("id", intent.getLongExtra("id", 0))
        startActivity(affiliate_service)
    }

    /*****************************showing contacts from db*****************************/
    fun show_bank_accounts(){

        object : AsyncTask<RecyclerView, Void, String>() {


            private var v: RecyclerView? = null
            private var message: String = ""
            private var bank_accounts: ArrayList<MyBanks> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    bank_accounts = database?.myBanksDao()?.getBanksByBankByUser(0, intent.getLongExtra("id", 0)) as ArrayList<MyBanks>
                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }


            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList(bank_accounts)

                    if(!(bank_accounts == null || bank_accounts.size > 0)){
                        no_banks.visibility = View.VISIBLE
                    }else{
                        no_banks.visibility = View.INVISIBLE
                    }

                } else {

                    //v!!.visibility = View.GONE
                    no_banks.visibility = View.VISIBLE
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, banks_list)


    }

    /*****************************receiving new contacts in db and updating contacts UI*****************************/
    private inner class ServiceBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                NEW_SERVICE_ADDED -> show_bank_accounts()
            }

        }
    }

    /*****************************updating contacts UI*****************************/
    /*fun updateServices() {
        val contacts = contactQuery.find()
        adapter?.setContacts(contacts = contacts as ArrayList<Contact>)
        val items = adapter?.itemCount
        if(!(items == null || items <= 0)){
            no_contacts_?.visibility = View.INVISIBLE
        }
        Log.v(TAG, "update contacts called")
    }*/

    override fun onStart() {
        applicationContext.registerReceiver(serviceReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        super.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        applicationContext.unregisterReceiver(serviceReceiver)
    }

    fun refreshList(bank_accounts: ArrayList<MyBanks>){

        Log.v(TAG, "refreshList called")

        adapter.updateData(bank_accounts).notifyDataSetChanged()
    }

    private val adapter by lazy {
        SlimAdapter.create()
                .register<MyBanks>(R.layout.bank) { data, injector ->
                    injector
                            .text(R.id.bank_name, data.bank_name)
                            .text(R.id.bank_fl, data.bank_name.first().toUpperCase().toString())
                            .text(R.id.bank_account, data.user_account)

                }
                .attachTo(recyclerView)
    }


    private val recyclerView: RecyclerView by lazy<RecyclerView> {
        banks_list.apply { // uxStores is the recyclerview’s name
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(this@Banks_Screen, LinearLayoutManager.VERTICAL))

        }!!
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.getItemId()) {
            R.id.add_button -> {
                goToAddBank()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
