package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MyInstitutions constructor(
        @PrimaryKey (autoGenerate = true) var id: Long = 0,
        var institution_id: String = "",
        var user_passport: String = "",
        var user_id: Long = 0) {
}