package cortez.citerio.com.gestordigital.util

import android.content.Context
import android.util.TypedValue

object AndroidUtils {
    fun sp2px(value: Float, context: Context) =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    value, context.resources.displayMetrics)

    fun dp2px(value: Float, context: Context) =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    value, context.resources.displayMetrics)
}