package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface MyTCsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTC(tc: MyTCs)

    @Query("SELECT * FROM mytcs WHERE id = :id")
    fun getTC(id: Long): MyTCs

    @Query("SELECT * FROM mytcs WHERE user_id = :user")
    fun getTCsByUser(user: Long): List<MyTCs>

    @Query("DELETE FROM mytcs")
    fun removeAllMyTCs()

    @Update
    fun updateTC(bank: MyTCs)

    @Delete
    fun deleteTC(tc: MyTCs)

}