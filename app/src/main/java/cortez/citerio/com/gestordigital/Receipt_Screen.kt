package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Bundle
import android.util.Log
import cortez.citerio.com.gestordigital.model.Preference
import kotlinx.android.synthetic.main.receipt_screen.*
import org.apache.commons.text.StringEscapeUtils


class Receipt_Screen: BaseActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.receipt_screen)

        val data = intent.getStringExtra("htmlReceipt")
        receipt_wv.loadData(StringEscapeUtils.unescapeHtml4(data), "text/html", "UTF-8")
        //Log.v("html", StringEscapeUtils.unescapeHtml4(data))
        val intent_ma = Intent("cortez.citerio.com.gestordigital")
        intent_ma.putExtra("RESULT_CODE", "NEW_INVOICE")
        applicationContext.sendBroadcast(intent_ma)

        affiliate_button.setOnClickListener {
            finish()
        }
    }

    fun goToMain(){
        val intentToMain = Intent(this@Receipt_Screen, Tabs_Screen::class.java)
        startActivity(intentToMain)
        finish()
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Receipt_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@Receipt_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }
}