package cortez.citerio.com.gestordigital.model

data class Notification constructor(
        var id : Int = 0,
        var invoiceId  : String = "",
        var invoiceNumber : String = "",
        var invoiceName : String = "",
        var invoiceAmount : String = "",
        var invoiceDate : String = "",
        var invoiceDueDate : String = "",
        var invoiceDescription : String = ""
){

}