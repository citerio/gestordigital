package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User constructor(
        @PrimaryKey var id: Long = 0,
        var name: String = "",
        var last_name: String = "",
        var email: String =  ""
) {

}