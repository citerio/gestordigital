package cortez.citerio.com.gestordigital.util

import android.app.*
import android.content.BroadcastReceiver
import android.os.Build
import android.content.Context.NOTIFICATION_SERVICE
import android.media.RingtoneManager
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.messaging.RemoteMessage
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.gson.Gson
import cortez.citerio.com.gestordigital.*
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.model.Notification
import kotlinx.android.synthetic.main.new_tc_screen.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MyFirebaseMessagingService : FirebaseMessagingService() {


    val GROUP_KEY_WORK_EMAIL = "cortez.citerio.com.gestordigital"
    val SUMMARY_ID = 0
    var notificationsList = ArrayList<Notification>()
    private val FINISH_ACTIVITY = "FINISH"
    //private var finishReceiver: ActivityBroadCastReceiver? = null

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.i("mesaje que llego", "From: " + remoteMessage!!.from!!)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            val data = JSONObject(remoteMessage.data)

            addInvoiceToDB(data)
            //sendNotification(data)
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body!!)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    /*private fun scheduleJob() {
        // [START dispatch_job]
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val myJob = dispatcher.newJobBuilder()
                .setService(MyJobService::class.java)
                .setTag("my-job-tag")
                .build()
        dispatcher.schedule(myJob)
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(data: JSONObject, id: Int) {
        val df = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        val sf = SimpleDateFormat("dd/MM/yyyy")

        val date = df.parse(data.getString("date"))
        val dueDate = df.parse(data.getString("dueDate"))

        val notificationList = Preference.getString(this,"notificationList")
        val notifications = Gson().fromJson(notificationList, Array<Notification>::class.java).toList()

        notificationsList.addAll(notifications)

        notificationsList.add(Notification(id = id, invoiceId = data.getString("id"), invoiceAmount = data.getString("amount"), invoiceDate = sf.format(date), invoiceDueDate = sf.format(dueDate), invoiceDescription = data.getString("description"), invoiceName = data.getString("name"), invoiceNumber = data.getString("number")))

        val notificationGsonList = Gson().toJson(notificationsList)
        Preference.saveString(applicationContext,"notificationList", notificationGsonList)
        showNotifications()


    }

    fun showNotifications(){

        val channelId = getString(R.string.university)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val inboxStyle = NotificationCompat.InboxStyle()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        Log.v(TAG, "Notifications ${notificationsList.size}")

        notificationsList.reversed().forEach {

            val intentPay = Intent(this, Payment_Screen::class.java)
            intentPay.putExtra("id", it.invoiceId)
            intentPay.putExtra("name", it.invoiceName)
            intentPay.putExtra("amount", it.invoiceAmount)
            intentPay.putExtra("number", it.invoiceNumber)
            intentPay.putExtra("date", it.invoiceDate)
            intentPay.putExtra("dueDate", it.invoiceDueDate)
            intentPay.putExtra("description", it.invoiceDescription)
            intentPay.putExtra("notification", it.id)

            val pendingIntentPay: PendingIntent? = TaskStackBuilder.create(baseContext)
                    .addNextIntentWithParentStack(intentPay)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)


            val intentDismissed = Intent(this, NotificationDismissedReceiver::class.java)
            intentDismissed.putExtra("notification", it.id)

            val pendingIntentDismissed = PendingIntent.getBroadcast(this, it.id, intentDismissed, 0)


            val messageTitle = "Tienes una factura de " + it.invoiceName
            val messageBody = "Por motivo de: " + it.invoiceDescription + ".\nMonto:" + it.invoiceAmount
            val notification = NotificationCompat.Builder(this@MyFirebaseMessagingService, channelId)
                    .setSmallIcon(R.mipmap.baseline_receipt_white_24)
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(messageBody))
                    //.setStyle(NotificationCompat.InboxStyle())
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntentPay)
                    .addAction(R.mipmap.baseline_credit_card_white_36, getString(R.string.pay), pendingIntentPay)
                    .setDeleteIntent(pendingIntentDismissed)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    .build()

            inboxStyle.addLine(it.invoiceName)

            NotificationManagerCompat.from(this).apply {
                notify(it.id, notification)
            }



        }

        if (notificationsList.size > 1){

            val intentTransactions = Intent(this, Tabs_Screen::class.java)

            val pendingIntentTransactions: PendingIntent? = TaskStackBuilder.create(baseContext)
                    .addNextIntentWithParentStack(intentTransactions)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

            val intentDismissed = Intent(this, NotificationDismissedReceiver::class.java)
            intentDismissed.putExtra("notification", 0)

            val pendingIntentDismissed = PendingIntent.getBroadcast(this, 0, intentDismissed, 0)

            val summaryNotification = NotificationCompat.Builder(this@MyFirebaseMessagingService, channelId)
                    .setContentTitle("Sistema Virtual")
                    //set content text to support devices running API level < 24
                    .setContentText("${notificationsList.size} facturas pendientes")
                    .setContentIntent(pendingIntentTransactions)
                    .setDeleteIntent(pendingIntentDismissed)
                    .setAutoCancel(true)
                    .setSmallIcon(R.mipmap.gestordigital_small)
                    //build summary info into InboxStyle template
                    .setStyle(inboxStyle)
                    //specify which group this notification belongs to
                    .setGroup(GROUP_KEY_WORK_EMAIL)
                    //set this notification as the summary for the group
                    .setGroupSummary(true)
                    .build()

            NotificationManagerCompat.from(this).apply {
                notify(SUMMARY_ID, summaryNotification)
            }

        }


    }

    /*****************************adding a new invoice to DB*****************************/
    private fun addInvoiceToDB(data: JSONObject){

        object : AsyncTask<String, Void, String>() {


            private var message = ""
            private var notification = 0

            override fun doInBackground(vararg params: String): String {

                try {
                    notification = Random().nextInt()
                    Log.v(TAG, "notification on Firebase: $notification")
                    Log.e("date", data.getString("date"))
                    Log.e("dueDate", data.getString("dueDate"))
                    var invoice  = MyInvoices( id = data.getString("id"), number = data.getString("number"), amount = data.getString("amount"), date = data.getString("date"), dueDate = data.getString("dueDate"), description = data.getString("description"), status = data.getString("status").toInt(), institution_id =  data.getString("institutionId"), notification = notification)
                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myInvoicesDao()?.addInvoice(invoice)
                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {
                    Log.v(TAG, "New invoice inserted in DB")
                    sendNotification(data = data, id = notification)
                    val intent_ma = Intent("cortez.citerio.com.gestordigital")
                    intent_ma.putExtra("RESULT_CODE", "NEW_INVOICE")
                    applicationContext.sendBroadcast(intent_ma)

                } else {

                    Log.v(TAG, "Error on inserting new invoice in DB")

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }

    companion object {

        private val TAG = "SistemaVirtualApp"
    }

    /*override fun onCreate() {
        super.onCreate()
        finishReceiver = ActivityBroadCastReceiver()
    }*/



    /*/*****************************receiving finish() command from base activity*****************************/
    private inner class ActivityBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                FINISH_ACTIVITY -> finish()
            }

        }
    }*/

    override fun onDestroy() {
        super.onDestroy()
        Log.v(TAG, "onDestroy Firebase Service called")
    }



}