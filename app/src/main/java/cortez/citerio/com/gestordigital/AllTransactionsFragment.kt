package cortez.citerio.com.gestordigital

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cortez.citerio.com.gestordigital.util.InjectorUtils
import cortez.citerio.com.gestordigital.viewmodel.MyInvoiceViewModel
import kotlinx.android.synthetic.main.fragment_transactions_all.*


class AllTransactionsFragment : androidx.fragment.app.Fragment() {

    internal var position: Int = 0
    private val TAG = "SistemaVirtualApp"
    var adapter: TransactionAdapter? = null
    var progressbar: ProgressBar? = null
    private val NEW_INVOICE = "NEW_INVOICE"
    private var finishReceiver: ActivityBroadCastReceiver? = null
    private lateinit var viewModel: MyInvoiceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(TAG, "onCreate Alltransactions called")
        finishReceiver = ActivityBroadCastReceiver()
        activity?.registerReceiver(finishReceiver, IntentFilter("cortez.citerio.com.gestordigital"))
        position = arguments!!.getInt("pos")
        val factory = InjectorUtils.provideMyInvoiceViewModelFactory(requireActivity())
        viewModel = ViewModelProviders.of(this, factory).get(MyInvoiceViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_transactions_all, container,false)
        val rv = rootView.findViewById<RecyclerView>(R.id.receipts_list)
        rv.layoutManager = LinearLayoutManager(context)
        adapter = TransactionAdapter()
        rv.adapter = adapter
        show_invoices(position)
        Log.v(TAG, "onViewCreated called")

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Creates a vertical Layout Manager
        //rv_transactions_list.layoutManager = LinearLayoutManager(context)


    }


    fun show_invoices(position: Int){

        when(position) {
            0 -> {
                viewModel.getAllInvoicesOrderByDate().observe(viewLifecycleOwner, Observer { invoices ->
                    if (invoices != null){
                        showEmptyList(invoices.size == 0)
                        adapter?.submitList(invoices)
                    }
                })
            }
            else -> {
                viewModel.getInvoicesByStatusOrderByDate(position - 1).observe(viewLifecycleOwner, Observer { invoices ->
                    if (invoices != null){
                        showEmptyList(invoices.size == 0)
                        adapter?.submitList(invoices)
                    }
                })
            }
        }

    }


    private fun showEmptyList(show: Boolean) {
        if (show) {
            empty_transactions_list.visibility = View.VISIBLE
            receipts_list.visibility = View.GONE
        } else {
            empty_transactions_list.visibility = View.GONE
            receipts_list.visibility = View.VISIBLE
        }
    }

    fun refreshList(){
        adapter?.notifyDataSetChanged()
    }


    companion object {
        fun getInstance(position: Int, texto: String): androidx.fragment.app.Fragment {
            val bundle = Bundle()
            bundle.putInt("pos", position)
            bundle.putString("tex", texto)
            val tabFragment = AllTransactionsFragment()
            tabFragment.arguments = bundle
            return tabFragment
        }
    }


    override fun onDestroy() {
        activity?.unregisterReceiver(finishReceiver)
        super.onDestroy()
    }

    /*****************************receiving finish() command from base activity*****************************/
    private inner class ActivityBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                //NEW_INVOICE -> show_invoices(position)
            }

        }
    }
}