package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface ProviderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProvider(provider: Provider)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProviders(provider: List<Provider>)

    /*@Query("SELECT * FROM provider WHERE id = :id")
    fun getProvider(id: Int): Provider*/

    @Query("SELECT * FROM provider WHERE type = :type")
    fun getProvider(type: Long): List<Provider>

    /*@Query("SELECT * FROM user WHERE email = :email AND password = :password")
    fun getUser(email: String, password: String): User*/

    @Query("DELETE FROM provider")
    fun removeAllProviders()

    @Update
    fun updateProvider(provider: Provider)

}