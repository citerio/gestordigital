package cortez.citerio.com.gestordigital.volley

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import cortez.citerio.com.gestordigital.model.Notification
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.util.ForegroundBackgroundListener
import cortez.citerio.com.gestordigital.util.LogoutListener


class BaseApplication : Application() {

    private lateinit var mHandler: Handler
    private val TAG = "SistemaVirtualApp"
    private lateinit var mRunnable: Runnable
    private lateinit var listener : LogoutListener
    private val lifeCycleLister : ForegroundBackgroundListener by lazy {
        object : ForegroundBackgroundListener{
            override fun startSomething() {
                super.startSomething()
                Log.v(TAG, "APP IS ON FOREGROUND")
                Preference.saveBoolean(applicationContext, "appON", true)
            }

            override fun stopSomething() {
                super.stopSomething()
                Log.v(TAG, "APP IS IN BACKGROUND")
                Preference.saveBoolean(applicationContext, "appON", false)
            }
        }
    }


    override fun onCreate() {
        super.onCreate()
        instance = this
        mHandler = Handler()
        mRunnable = Runnable {
            listener.onSessionLogout()
        }
        registerForegroundBackgroundListener()
        setNotificationList()
    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }

    companion object {
        private val TAG = BaseApplication::class.java.simpleName
        @get:Synchronized var instance: BaseApplication? = null
            private set
    }

    fun startUserSession(){

        cancelTimer()
        mHandler.postDelayed(mRunnable, 3000000)

    }

    fun cancelTimer(){
        mHandler.removeCallbacks(mRunnable)
    }

    fun registerSessionListener(listener : LogoutListener){
        this.listener = listener
    }

    fun registerForegroundBackgroundListener(){
        ProcessLifecycleOwner.get().lifecycle.addObserver(lifeCycleLister)
    }

    fun onUserInteracted(){
        startUserSession()
    }

    fun setNotificationList(){
        val notificationList = Preference.getString(applicationContext,"notificationList")
        if (notificationList.isEmpty()){
            val notificationArrayList = ArrayList<Notification>()
            val notificationGsonList = Gson().toJson(notificationArrayList)
            Preference.saveString(applicationContext,"notificationList", notificationGsonList)
        }
    }
}