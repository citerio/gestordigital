package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MyServices constructor(
        @PrimaryKey (autoGenerate = false) var id: String = "",
        var provider_type: Long = 0,
        var provider_name: String = "",
        var provider_id: Long = 0,
        var user_contract: String = "",
        var user_passport: String = "",
        var user_id: Long = 0) {
}