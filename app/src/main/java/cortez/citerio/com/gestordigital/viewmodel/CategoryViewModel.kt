package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cortez.citerio.com.gestordigital.model.Category
import cortez.citerio.com.gestordigital.model.CategoryRepository

class CategoryViewModel internal constructor(private val categoryRepository: CategoryRepository) : ViewModel(){

    fun getCategories() = categoryRepository.getCategories()
}