package cortez.citerio.com.gestordigital

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_transactions_all.*

class PendingTransactionsFragment : androidx.fragment.app.Fragment() {

    internal var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = arguments!!.getInt("pos")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transactions_pending, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*textView!!.text = "Fragment "
        holacara!!.text = "Texto "*/

    }

    companion object {

        fun getInstance(position: Int): androidx.fragment.app.Fragment {
            val bundle = Bundle()
            bundle.putInt("pos", position)
            val tabFragment = PendingTransactionsFragment()
            tabFragment.arguments = bundle
            return tabFragment
        }
    }
}