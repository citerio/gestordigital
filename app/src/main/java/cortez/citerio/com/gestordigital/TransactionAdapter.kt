package cortez.citerio.com.gestordigital

import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.*
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import cortez.citerio.com.gestordigital.model.InvoiceInst
import cortez.citerio.com.gestordigital.model.MyInvoices
import cortez.citerio.com.gestordigital.model.Transaction
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*




class TransactionAdapter : PagedListAdapter<InvoiceInst, RecyclerView.ViewHolder>(INVOICE_COMPARATOR) {

    // Gets the number of transactions in the list


    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //return RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.transaction_paid_item, parent, false))
        return if(viewType == 0) {
            TransactionPendingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.transaction_pending_item, parent, false))
        } else {
            TransactionPaidViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.transaction_paid_item, parent, false))
        }
    }

    // Binds each transaction in the ArrayList to a view
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemType = getItemViewType(position)

        val df = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        val sf = SimpleDateFormat("MMM dd, yyyy")

        val date = df.parse(getItem(position)!!.date)
        val dueDate = df.parse(getItem(position)!!.dueDate)

        val currentDate = Date()

        val diff = dueDate.time - currentDate.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24

        var timeRemaining = ""

        if(days > 0) {
            timeRemaining = days.toString() + " dias"
        } else {
            timeRemaining = hours.toString() + " horas"
        }


        if(itemType == 0) {
            val transactionPendingHolder: TransactionPendingViewHolder? = holder as? TransactionPendingViewHolder
            //transactionPendingHolder?.pend_inst_icon?.setImageResource(res.getIdentifier("cortez.citerio.com.gestordigital:mipmap/" + icon, null, null))
            Log.e("cateName", getItem(position)!!.cateName)
            Log.e("instName", getItem(position)!!.instName)
            transactionPendingHolder?.pend_provider_fl?.text = getItem(position)!!.instName.take(1)
            transactionPendingHolder?.pend_inst_cat?.text = getItem(position)!!.cateName
            transactionPendingHolder?.pend_inst_name?.text = getItem(position)!!.instName
            transactionPendingHolder?.pend_amount?.text = getItem(position)!!.amount
            transactionPendingHolder?.date?.text = sf.format(date)
//            transactionPendingHolder?.pend_days_left?.text = (dueDate.date - date.date).toString() + " dias"
            transactionPendingHolder?.pend_days_left?.text = timeRemaining
            transactionPendingHolder?.go_to_payment?.setOnClickListener {
                val payment_screen = Intent(transactionPendingHolder.itemView.context, Payment_Screen::class.java)
                payment_screen.putExtra("id", getItem(position)!!.id)
                payment_screen.putExtra("category", getItem(position)!!.cateName)
                payment_screen.putExtra("name", getItem(position)!!.instName)
                payment_screen.putExtra("amount", getItem(position)!!.amount)
                payment_screen.putExtra("date", sf.format(date))
                payment_screen.putExtra("dueDate", sf.format(dueDate))
                payment_screen.putExtra("description", getItem(position)!!.description)
                payment_screen.putExtra("number", getItem(position)!!.number)
                payment_screen.putExtra("notification", getItem(position)!!.notification)
                transactionPendingHolder.itemView.context.startActivity(payment_screen)
            }
            transactionPendingHolder?.go_to_detail?.setOnClickListener {
                val detail_screen = Intent(transactionPendingHolder.itemView.context, InvoiceDetail_Screen::class.java)
                detail_screen.putExtra("id", getItem(position)!!.id)
                detail_screen.putExtra("category", getItem(position)!!.cateName)
                detail_screen.putExtra("name", getItem(position)!!.instName)
                detail_screen.putExtra("amount", getItem(position)!!.amount)
                detail_screen.putExtra("date", sf.format(date))
                detail_screen.putExtra("dueDate", sf.format(dueDate))
                detail_screen.putExtra("description", getItem(position)!!.description)
                detail_screen.putExtra("number", getItem(position)!!.number)
                transactionPendingHolder.itemView.context.startActivity(detail_screen)
            }

        } else {
            val transactionPaidHolder: TransactionPaidViewHolder = holder as TransactionPaidViewHolder
            //transactionPaidHolder?.instIcon?.setImageResource(res.getIdentifier("cortez.citerio.com.gestordigital:mipmap/" + icon, null, null))
            transactionPaidHolder.instCat.text = getItem(position)!!.cateName
            transactionPaidHolder.instName.text = getItem(position)!!.instName
            transactionPaidHolder.amount.text = getItem(position)!!.amount
            transactionPaidHolder.date?.text = sf.format(date)
            transactionPaidHolder.go_to_detail?.setOnClickListener {
                val detail_screen = Intent(transactionPaidHolder.itemView.context, InvoiceDetail_Screen::class.java)
                detail_screen.putExtra("id", getItem(position)!!.id)
                detail_screen.putExtra("category", getItem(position)!!.cateName)
                detail_screen.putExtra("name", getItem(position)!!.instName)
                detail_screen.putExtra("amount", getItem(position)!!.amount)
                detail_screen.putExtra("date", sf.format(date))
                detail_screen.putExtra("dueDate", sf.format(dueDate))
                detail_screen.putExtra("description", getItem(position)!!.description)
                detail_screen.putExtra("number", getItem(position)!!.number)
                transactionPaidHolder.itemView.context.startActivity(detail_screen)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)!!.status
    }

    class TransactionPaidViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        //val instIcon = itemView.findViewById<ImageView>(R.id.instIcon)
        val instCat = itemView.findViewById<TextView>(R.id.instCat)
        val instName = itemView.findViewById<TextView>(R.id.instName)
        val amount = itemView.findViewById<TextView>(R.id.amount)
        val date = itemView.findViewById<TextView>(R.id.date)
        val go_to_detail = itemView.findViewById<ImageButton>(R.id.go_to_detail)
    }

    class TransactionPendingViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        //val pend_inst_icon = itemView.findViewById<ImageView>(R.id.pend_inst_icon)
        val pend_provider_fl = itemView.findViewById<TextView>(R.id.provider_fl)
        val pend_inst_cat = itemView.findViewById<TextView>(R.id.pend_inst_cat)
        val pend_inst_name = itemView.findViewById<TextView>(R.id.pend_inst_name)
        val pend_amount = itemView.findViewById<TextView>(R.id.pend_amount)
        val pend_days_left = itemView.findViewById<TextView>(R.id.pend_days_left)
        val date = itemView.findViewById<TextView>(R.id.date)
        val go_to_payment = itemView.findViewById<Button>(R.id.go_to_payment)
        val go_to_detail = itemView.findViewById<Button>(R.id.go_to_detail)
    }

    companion object {
        private val INVOICE_COMPARATOR = object : DiffUtil.ItemCallback<InvoiceInst>() {
            override fun areItemsTheSame(oldItem: InvoiceInst, newItem: InvoiceInst): Boolean =
                    oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: InvoiceInst, newItem: InvoiceInst): Boolean =
                    oldItem == newItem
        }
    }
}