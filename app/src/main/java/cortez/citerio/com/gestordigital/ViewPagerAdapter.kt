package cortez.citerio.com.gestordigital

import android.content.Context
import android.os.AsyncTask
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import cortez.citerio.com.gestordigital.model.AppDatabase
import android.widget.TextView
import cortez.citerio.com.gestordigital.model.Preference
import kotlinx.android.synthetic.main.tab_design.view.*


internal class ViewPagerAdapter(manager: androidx.fragment.app.FragmentManager, context: Context?) : androidx.fragment.app.FragmentPagerAdapter(manager) {


    private val title = arrayOf(
            context?.resources?.getString(R.string.all),
            context?.resources?.getString(R.string.pending),
            context?.resources?.getString(R.string.bills_paid)
    )

    private val mContext = context

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return AllTransactionsFragment.getInstance(position, "hola")
    }

    override fun getCount(): Int {
        return title.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }

    fun getTabView(position: Int): View? {
        when (position) {
            0 -> {
                val view = LayoutInflater.from(mContext).inflate(R.layout.tab_design, null)
                view.title.text = mContext?.resources?.getString(R.string.all)
                return view
            }
            1 -> {
                val view = LayoutInflater.from(mContext).inflate(R.layout.tab_design, null)
                view.title.text = mContext?.resources?.getString(R.string.pending)
                return view
            }
            2 -> {
                val view = LayoutInflater.from(mContext).inflate(R.layout.tab_design, null)
                view.title.text = mContext?.resources?.getString(R.string.bills_paid)
                return view
            }
        }
        return null
    }




}