package cortez.citerio.com.gestordigital.model

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCategory(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCategories(categories: List<Category>)

    @Query("SELECT * FROM category WHERE id = :id")
    fun getCategory(id: String): Category

    @Query("SELECT * FROM category")
    fun getCategories(): LiveData<List<Category>>

    @Query("DELETE FROM category")
    fun removeAllCategories()

    @Update
    fun updateCategory(category: Category)

}