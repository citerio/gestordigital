package cortez.citerio.com.gestordigital.model

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface InstitutionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInstitution(institution: Institution)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInstitutions(institutions: List<Institution>)

    @Query("SELECT * FROM institution WHERE id = :id")
    fun getInstitution(id: String): Institution

    @Query("SELECT * FROM institution WHERE category_id = :category_id")
    fun getInstitutionsByCategory(category_id: String): LiveData<List<Institution>>

    @Query("DELETE FROM institution")
    fun removeAllInstitutions()

    @Update
    fun updateInstitution(institution: Institution)

}