package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface BankDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addBank(bank: Bank)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addBanks(bank: List<Bank>)

    @Query("SELECT * FROM bank WHERE id = :id")
    fun getBank(id: Int): Bank

    /*@Query("SELECT * FROM user WHERE email = :email AND password = :password")
    fun getUser(email: String, password: String): User*/

    @Query("DELETE FROM bank")
    fun removeAllBanks()

    @Update
    fun updateBank(bank: Bank)

}