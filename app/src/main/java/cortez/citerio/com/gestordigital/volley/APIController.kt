package cortez.citerio.com.gestordigital.volley

import android.content.Context
import android.util.Log
import com.android.volley.VolleyError
import org.json.JSONObject

class APIController constructor(serviceInjection: ServiceInterface): ServiceInterface {
    private val service: ServiceInterface = serviceInjection


    override fun getSimple(path: String, completionHandler: (response: JSONObject?) -> Unit) {
        service.getSimple(path, completionHandler)
    }

    override fun get(path: String, context: Context, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {
        service.get(path, context, completionHandler, errorHandler)
    }

    override fun post(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {
        service.post(path, context, params, completionHandler, errorHandler)
    }

    override fun postSimple(path: String, context: Context, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit, errorHandler: (error: VolleyError?) -> Unit) {
        service.post(path, context, params, completionHandler, errorHandler)
    }
}