package cortez.citerio.com.gestordigital.model

import androidx.lifecycle.LiveData
import androidx.room.*

data class MyInstitutionServ constructor(
        var id: String = "",
        var institution_id: String = "",
        var instName: String = "",
        var user_passport: String = ""
){
}

@Dao
interface MyInstitutionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addInstitution(institution: MyInstitutions)

    @Query("SELECT * FROM myinstitutions WHERE id = :id")
    fun getInstitution(id: String): MyInstitutions

    @Query("SELECT * FROM myinstitutions")
    fun getAllInstitutions(): List<MyInstitutions>

    @Query("SELECT mi.id, mi.institution_id, mi.user_passport, ins.name AS instName FROM myinstitutions mi JOIN institution ins ON mi.institution_id = ins.id where ins.category_id = :category_id")
    fun getMyInstitutionsByCategory(category_id: String): LiveData<List<MyInstitutionServ>>

    @Query("DELETE FROM myinstitutions")
    fun removeAllMyInstitutions()

    @Update
    fun updateInstitution(institution: MyInstitutions)

    @Delete
    fun deleteMyInstitution(institution: MyInstitutions)

}