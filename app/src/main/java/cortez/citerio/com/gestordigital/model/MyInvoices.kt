package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = arrayOf(ForeignKey(entity = Institution::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("institution_id"),
        onDelete = ForeignKey.CASCADE)))
data class MyInvoices constructor(
        @PrimaryKey val id: String,
        var number: String = "",
        var amount: String = "",
        var date: String = "",
        var dueDate: String = "",
        var description: String = "",
        var status: Int = 0,
        val institution_id: String,
        var notification: Int = 0
)
{
}