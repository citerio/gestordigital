package cortez.citerio.com.gestordigital

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.widget.TextView
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.util.InjectorUtils
import cortez.citerio.com.gestordigital.viewmodel.MyInvoiceViewModel
import kotlinx.android.synthetic.main.fragment_transactions.*
import kotlinx.android.synthetic.main.toolbar_tablayout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TransactionsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TransactionsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TransactionsFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var adapter: ViewPagerAdapter
    private lateinit var viewModel: MyInvoiceViewModel
    private val TAG = "SistemaVirtualApp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        Log.v(TAG, "TransactionsFragment OnCreate called")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        Log.v(TAG, "TransactionsFragment onCreateView called")
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    /*override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }*/

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.v(TAG, "TransactionsFragment onViewCreated called")
        val factory = InjectorUtils.provideMyInvoiceViewModelFactory(requireActivity())
        viewModel = ViewModelProviders.of(this, factory).get(MyInvoiceViewModel::class.java)
        adapter = ViewPagerAdapter(childFragmentManager, context)
        viewpager.adapter = adapter
        tablayout.setupWithViewPager(viewpager)
        for (i in 0 until tablayout.tabCount){
            val tab = tablayout.getTabAt(i)
            tab?.customView = adapter.getTabView(i)
        }
        updateBadge()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TransactionsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                TransactionsFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }




    fun updateBadge(){

        viewModel.getInvoicesByStatusCount(0).observe(viewLifecycleOwner, Observer { pendingInvoices ->
            val tab = tablayout.getTabAt(1)
            if (pendingInvoices != null) {
                if (pendingInvoices > 0){
                    tab?.customView?.findViewById<TextView>(R.id.count)?.text = pendingInvoices.toString()
                    tab?.customView?.findViewById<TextView>(R.id.count)?.visibility = View.VISIBLE
                }else{
                    tab?.customView?.findViewById<TextView>(R.id.count)?.visibility = View.GONE
                }
            }
        })

    }
}
