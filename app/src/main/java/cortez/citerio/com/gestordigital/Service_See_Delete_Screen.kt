package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import cortez.citerio.com.gestordigital.model.*
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.service_see_delete_screen.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject

class Service_See_Delete_Screen : BaseActivity() {

    private val TAG = "SistemaVirtualApp"
    private lateinit var service : MyInstitutions
    val serviceVolley = ServiceVolley()
    val apiController = APIController(serviceVolley)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_see_delete_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        user_id.keyListener = null
        //user_contract.keyListener = null

        delete_button.setOnClickListener{
            confirmDeleteService()
        }

        ok_button.setOnClickListener {
            finish()
        }

        getService()
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {

            when (view.id) {
                R.id.user_id -> validate_name()
                //R.id.user_contract -> validate_contract()
            }

        }
    }

    private fun validate_name(): Boolean {

        if (user_id.text.toString().isEmpty()) {

            user_id_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_id_layout.isErrorEnabled = false

        }

        return true

    }

    /*private fun validate_contract(): Boolean {

        if (user_contract.text.toString().isEmpty()) {

            user_contract_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_contract_layout.isErrorEnabled = false

        }

        return true

    }*/

    /*****************************adding a new service to DB*****************************/

    fun getService(){


        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    service = database?.myInstitutionsDao()?.getInstitution(intent.getStringExtra("id"))!!

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        if (service != null){
                            user_id.setText(service.user_passport)
                            //user_contract.setText(service.user_contract)
                        }else{
                            Log.v(TAG, "Service not found")
                        }

                        Log.v(TAG, "New service queried in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        //Toast.makeText(this@Service_See_Delete_Screen, "Nuevo comercio agregado", Toast.LENGTH_LONG).show()
                        //finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on querying new service in DB")
                    //Toast.makeText(this@Service_See_Delete_Screen, "ERROR inside adding service", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }

    fun deleteServiceFromServer() {
        val params = JSONObject()
        params.put("user_id", service.user_id)
        params.put("institution_id", service.institution_id)
        progressbar.visibility = View.VISIBLE
        delete_button.isEnabled = false

        apiController.post(EndPoints.URL_DELETE_SERVICE, this@Service_See_Delete_Screen, params,
            { response ->
                val result = response?.getString("result")
                if (result == "OK") {
                    deleteService()
                    /*val intent = Intent(this@New_Service_Screen, SignIn_Screen::class.java)
                    startActivity(intent)
                    finish()*/
                } else {
                    progressbar.visibility = View.GONE
                    delete_button.isEnabled = true
                    Toast.makeText(this@Service_See_Delete_Screen, "Ha ocurrido un error. Intente nuevamente",
                            Toast.LENGTH_SHORT).show();
                }
            },
            { error ->
                val networkResponse = error?.networkResponse
                var jsonError: String = ""
                var jsonObject: JSONObject? = null
                if (networkResponse != null && networkResponse.data != null) {
                    jsonError = String(networkResponse.data)
                    jsonObject = JSONObject(jsonError)

                    if(jsonObject?.has("errors")) {
                        showServerErrors(jsonObject?.getJSONArray("errors"))
                    } else {
                        Toast.makeText(this@Service_See_Delete_Screen, "Ha ocurrido un error. Intente nuevamente",
                                Toast.LENGTH_SHORT).show();
                    }
                    // Print Error!
                } else {
                    Toast.makeText(this@Service_See_Delete_Screen, "Ha ocurrido un error. Intente nuevamente",
                            Toast.LENGTH_SHORT).show();
                }
                delete_button.isEnabled = true
                progressbar.visibility = View.GONE
            })
    }

    fun deleteService(){

        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myInstitutionsDao()?.deleteMyInstitution(service)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        Log.v(TAG, "New service queried in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        Toast.makeText(this@Service_See_Delete_Screen, "Afiliacion Eliminada", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on deleting service in DB")
                    //Toast.makeText(this@Service_See_Delete_Screen, "ERROR inside adding service", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")

    }

    fun confirmDeleteService(){

        AlertDialog.Builder(this@Service_See_Delete_Screen)
                .setTitle("Confirme")
                .setMessage("¿Esta seguro de eliminar esta afiliacion?")
                .setPositiveButton(resources.getString(R.string.ok)) { _, _ ->
                    deleteServiceFromServer()
                    //deleteService()
                }
                .setNegativeButton("Cancel") { _, _ ->

                }.show()
    }


    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Service_See_Delete_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@Service_See_Delete_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }

    private fun showServerErrors(errors: JSONArray) {
        user_id_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }

    /*****************************receiving finish() command from baseactivity*****************************/
}
