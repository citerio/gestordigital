package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MyTCs constructor(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var card_number: String = "",
        var card_due_date: String = "",
        var card_cvc: String = "",
        var user_id: Long = 0,
        var user_name: String = ""
)
{
}