package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
data class Category constructor(
        @PrimaryKey val id: String,
        var name: String = "",
        var image: String = ""
)
{
}