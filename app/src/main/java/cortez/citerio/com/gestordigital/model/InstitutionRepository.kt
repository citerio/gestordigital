package cortez.citerio.com.gestordigital.model

class InstitutionRepository private constructor(private val institutionDao: InstitutionDao){


    fun getInstitutionsByCategory(category: String) = institutionDao.getInstitutionsByCategory(category)

    fun addInstitutions(institutionsList: List<Institution>){
        institutionDao.addInstitutions(institutionsList)
    }

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: InstitutionRepository? = null

        fun getInstance(institutionDao: InstitutionDao) =
                instance ?: synchronized(this) {
                    instance ?: InstitutionRepository(institutionDao).also { instance = it }
                }
    }
}