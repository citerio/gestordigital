package cortez.citerio.com.gestordigital

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cortez.citerio.com.gestordigital.model.Category
import cortez.citerio.com.gestordigital.util.InjectorUtils
import cortez.citerio.com.gestordigital.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_services.*
import net.idik.lib.slimadapter.SlimAdapter
import net.idik.lib.slimadapter.viewinjector.IViewInjector


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ServicesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ServicesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ServicesFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters

    private val VERTICAL_ITEM_SPACE = 48
    private var param1: String? = null
    private var param2: String? = null
    private val TAG = "SistemaVirtualApp"
    private var listener: OnFragmentInteractionListener? = null
    //var adapter: CategoryAdapter? = null
    private val categories: ArrayList<Category> = ArrayList()
    private lateinit var viewModel: CategoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        Log.v(TAG, "ServicesFragment OnCreate called")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        Log.v(TAG, "ServicesFragment onCreateView called")

        return inflater.inflate(R.layout.fragment_services, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.v(TAG, "ServicesFragment onViewCreated called")

        val factory = InjectorUtils.provideCategoryViewModelFactory(requireActivity())
        viewModel = ViewModelProviders.of(this, factory).get(CategoryViewModel::class.java)

        /*categories_rv.layoutManager = LinearLayoutManager(context)
        categories_rv.addItemDecoration(DividerItemDecoration(context!!,
                DividerItemDecoration.VERTICAL))
        adapter = CategoryAdapter(categories, context, resources)
        categories_rv.adapter = adapter*/
        show_categories()

    }

    private val adapter by lazy {
        SlimAdapter.create()
                .register<Category>(R.layout.category_list_item) { data, injector ->
                    injector
                            .text(R.id.category_name, data.name)
                            .clicked(R.id.category_item){
                                Log.e("capturo el click", "capturo el click")
                                val services_screen = Intent(context, Services_Screen::class.java)
                                services_screen.putExtra("categoryId", data.id)
                                context?.startActivity(services_screen)
                            }.with(R.id.category_name, IViewInjector.Action<TextView> {
                                when(data.name){
                                    "Cable" -> {
                                        val drawable = ContextCompat.getDrawable(activity!!.applicationContext, R.mipmap.baseline_live_tv_black_36)
                                        it.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
                                    }
                                    "gimnasio" -> {
                                        val drawable = ContextCompat.getDrawable(activity!!.applicationContext, R.mipmap.baseline_fitness_center_black_36)
                                        it.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
                                    }
                                    "Colegiatura" -> {
                                        val drawable = ContextCompat.getDrawable(activity!!.applicationContext, R.mipmap.outline_school_black_36)
                                        it.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
                                    }
                                }
                            })
                }
                .attachTo(recyclerView)


    }

    private val recyclerView: RecyclerView by lazy<RecyclerView> {
        categories_rv.apply { // uxStores is the recyclerview’s name
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))

        }!!
    }

    /*fun show_categories(){

        object : AsyncTask<RecyclerView, Void, String>() {
            private var v: RecyclerView? = null
            private var message: String = ""
            private var categories_db: ArrayList<Category> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {

                    val database = AppDatabase.getDatabase(activity!!.baseContext)


                    categories_db = database?.categoryDao()?.getCategories() as ArrayList<Category>

                    categories.clear()
                    categories.addAll(categories_db)

                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }


            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, rv_transactions_list)
    }*/

    fun show_categories(){
        viewModel.getCategories().observe(viewLifecycleOwner, Observer { cats ->
            if (cats != null){
                adapter.updateData(cats).notifyDataSetChanged()
            }
        })
    }

    fun refreshList(){
        adapter?.notifyDataSetChanged()
    }

    fun goToProviderType(type: Long){
        val services_screen = Intent(activity, Services_Screen::class.java)
        services_screen.putExtra("type", type)
        startActivity(services_screen)
    }

    fun goToBank(id: Long, name:String){
        val bank_screen = Intent(activity, Banks_Screen::class.java)
        bank_screen.putExtra("id", id)
        bank_screen.putExtra("name", name)
        startActivity(bank_screen)
    }

    fun goToTC(id: Long, name:String){
        val tcs_screen = Intent(activity, TCs_Screen::class.java)
        //bank_screen.putExtra("id", id)
        //bank_screen.putExtra("name", name)
        startActivity(tcs_screen)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    /*override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }*/

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ServicesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ServicesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
