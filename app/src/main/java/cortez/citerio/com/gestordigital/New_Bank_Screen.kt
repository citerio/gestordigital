package cortez.citerio.com.gestordigital

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.MyBanks
import kotlinx.android.synthetic.main.new_bank_screen.*

class New_Bank_Screen : AppCompatActivity() {

    private val TAG = "SistemaVirtualApp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_bank_screen)

        setSupportActionBar(toolbar_new_bank)
        supportActionBar?.title = ""

        user_id.addTextChangedListener(MyTextWatcher(user_id))
        user_account.addTextChangedListener(MyTextWatcher(user_account))

        affiliate_button.setOnClickListener{


            if(!user_id.text.toString().trim().equals("") && !user_account.text.toString().trim().equals("") ){

                addBankAccount()

            }else{

                Toast.makeText(this@New_Bank_Screen, resources.getString(R.string.empty_field), Toast.LENGTH_LONG).show()
            }
        }




        cancel_button.setOnClickListener {
            finish()
        }
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {

            when (view.id) {
                R.id.user_id -> validate_name()
                R.id.user_account -> validate_account()
            }

        }
    }

    private fun validate_name(): Boolean {

        if (user_id.text.toString().isEmpty()) {

            user_id_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_id_layout.isErrorEnabled = false

        }

        return true

    }

    private fun validate_account(): Boolean {

        if (user_account.text.toString().isEmpty()) {

            user_account_layout.error = getString(R.string.empty_field)

            return false

        } else {

            user_account_layout.isErrorEnabled = false

        }

        return true

    }

    /*****************************adding a new service to DB*****************************/
    fun addBankAccount(){


        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val bank_account  = MyBanks( bank_name = intent.getStringExtra("name"), bank_id = intent.getLongExtra("id", 0), user_account = user_account.text.toString(), user_id = 0, user_passport = user_id.text.toString() )
                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myBanksDao()?.addBank(bank_account)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {

                    try {

                        Log.v(TAG, "New bank account inserted in DB")
                        //intent_ma.putExtra("RESULT_CODE", "NEW_CONTACT_ADDED")
                        //applicationContext.sendBroadcast(intent_ma)
                        Toast.makeText(this@New_Bank_Screen, "Nueva cuenta bancaria agregada", Toast.LENGTH_LONG).show()
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on inserting new bank account in DB")
                    Toast.makeText(this@New_Bank_Screen, "ERROR inside adding bank account", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")


    }
}
