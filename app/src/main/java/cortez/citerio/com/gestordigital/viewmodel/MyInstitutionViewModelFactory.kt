package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cortez.citerio.com.gestordigital.model.InstitutionRepository
import cortez.citerio.com.gestordigital.model.MyInstitutionRepository

class MyInstitutionViewModelFactory (private val myInstitutionRepository: MyInstitutionRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyInstitutionViewModel(myInstitutionRepository) as T
    }

}