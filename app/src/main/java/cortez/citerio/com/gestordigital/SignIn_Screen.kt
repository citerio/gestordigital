package cortez.citerio.com.gestordigital

import android.app.Activity
import android.app.TaskStackBuilder
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.signin_screen.*
import org.json.JSONObject


class SignIn_Screen : AppCompatActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signin_screen)

        password1?.addTextChangedListener(MyTextWatcher(password1))
        email1?.addTextChangedListener(MyTextWatcher(email1))

        //Log.e("viendo", intent.getStringExtra("email"))
        if(intent.hasExtra("email")) {
            email1.setText(intent.getStringExtra("email"))
        }


        signin_button.setOnClickListener{
            progressbar.visibility = View.VISIBLE
            signin_button.isEnabled = false

            var valid = true
            if(TextUtils.isEmpty(email1.text.toString())){
                email_layout1?.error = getString(R.string.empty_field)
                email_layout1?.isErrorEnabled = true
                valid = false
            }
            if(TextUtils.isEmpty(password1.text.toString())){
                password_layout1?.error = getString(R.string.empty_field)
                password_layout1?.isErrorEnabled = true
                valid = false
            }

            if(valid) {
                val params = JSONObject()
                params.put("email", email1?.text.toString())
                params.put("password", password1?.text.toString())

                apiController.post(EndPoints.URL_LOGIN, this@SignIn_Screen, params,
                    { response ->
                        val result = response?.getString("result")
                        if (result == "OK") {
                            Preference.saveString(applicationContext, "usertoken", response?.getString("token"))

                            var intentNext: Intent? = null
                            if(intent!!.hasExtra("nextView")) {
                                if(intent!!.getStringExtra("nextView") == "detail") {
                                    intentNext = Intent(this@SignIn_Screen, InvoiceDetail_Screen::class.java)
                                } else {
                                    intentNext = Intent(this@SignIn_Screen, Payment_Screen::class.java)
                                    intentNext.putExtra("category", intent.getIntExtra("category", 0))
                                }

                                intentNext.putExtra("id", intent.getStringExtra("id"))
                                intentNext.putExtra("name", intent.getStringExtra("name"))
                                intentNext.putExtra("amount", intent.getStringExtra("amount"))
                                intentNext.putExtra("number", intent.getStringExtra("number"))
                                intentNext.putExtra("date", intent.getStringExtra("date"))
                                intentNext.putExtra("dueDate", intent.getStringExtra("dueDate"))
                                intentNext.putExtra("description", intent.getStringExtra("description"))
                                val sBuilder = TaskStackBuilder.create(this@SignIn_Screen)
                                sBuilder.addNextIntentWithParentStack(intentNext)
                                sBuilder.startActivities()
                                finish()
                            } else {
                                intentNext = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
                                setResult(Activity.RESULT_OK, null)
                                startActivity(intentNext)
                                finish()
                            }
                        } else {
                            progressbar.visibility = View.GONE
                            signin_button.isEnabled = true
                            Toast.makeText(this@SignIn_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }

                    },
                    { error ->
                        val networkResponse = error?.networkResponse
                        var jsonError: String = ""
                        var jsonObject: JSONObject? = null
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = String(networkResponse.data)
                            jsonObject = JSONObject(jsonError)

                            if(jsonObject?.has("message") && jsonObject?.getString("message") == "Incorrect email or password.") {
                                wrong_credentials.visibility = View.VISIBLE
                                progressbar.visibility = View.GONE
                                signin_button?.isEnabled = true
                                //showServerErrors(jsonObject?.getJSONArray("errors"))
                            } else {
                                Toast.makeText(this@SignIn_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            // Print Error!
                        } else {
                            Toast.makeText(this@SignIn_Screen, "Ocurrio un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        progressbar.visibility = View.GONE
                        signin_button.isEnabled = true
                    })

                /*val que = Volley.newRequestQueue(this@SignIn_Screen)
                val req = object: StringRequest(Request.Method.POST, EndPoints.URL_LOGIN,
                    Response.Listener {
                        response ->
                        val response = JSONObject(response)
                        Preference.saveString(applicationContext, "usertoken", response.getString("token"))

                        var intentNext: Intent? = null
                        if(intent!!.hasExtra("nextView")) {
                            if(intent!!.getStringExtra("nextView") == "detail") {
                                intentNext = Intent(this@SignIn_Screen, InvoiceDetail_Screen::class.java)
                            } else {
                                intentNext = Intent(this@SignIn_Screen, Payment_Screen::class.java)
                                intentNext.putExtra("category", intent.getIntExtra("category", 0))
                            }

                            intentNext.putExtra("id", intent.getStringExtra("id"))
                            intentNext.putExtra("name", intent.getStringExtra("name"))
                            intentNext.putExtra("amount", intent.getStringExtra("amount"))
                            intentNext.putExtra("number", intent.getStringExtra("number"))
                            intentNext.putExtra("date", intent.getStringExtra("date"))
                            intentNext.putExtra("dueDate", intent.getStringExtra("dueDate"))
                            intentNext.putExtra("description", intent.getStringExtra("description"))
                            val sBuilder = TaskStackBuilder.create(this@SignIn_Screen)
                            sBuilder.addNextIntentWithParentStack(intentNext)
                            sBuilder.startActivities()
                            finish()
                        } else {
                            intentNext = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
                            setResult(Activity.RESULT_OK, null)
                            startActivity(intentNext)
                            finish()
                        }


                    }, Response.ErrorListener {
                        error ->

                        wrong_credentials.visibility = View.VISIBLE
                        progressbar.visibility = View.GONE
                        signin_button?.isEnabled = true
                    }) {
                        @Throws(AuthFailureError::class)
                        override fun getParams(): Map<String, String> {
                            val params = HashMap<String, String>()
                            params.put("email", email1?.text.toString())
                            params.put("password", password1?.text.toString())
                            return params
                        }
                    }

                que.add(req)*/
            } else {
                progressbar.visibility = View.GONE
                signin_button?.isEnabled = true
            }
        }

        forgot_password.setOnClickListener{
            val intent = Intent(this@SignIn_Screen, ForgotPassword_Screen::class.java)
            startActivity(intent)
        }
    }

    public override fun onStart() {
        super.onStart()
        if(Preference.getString(applicationContext, "usertoken") != ""){
            val intentToMain = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }
    }
    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (wrong_credentials.isShown){
                wrong_credentials.visibility = View.GONE
            }
        }

        override fun afterTextChanged(s: Editable) {
            Log.e("info", "paso por aqui")
            when (view.id) {
                R.id.email1 -> validate_email()
                R.id.password1 -> validate_password()
            }

        }
    }

    private fun validate_email(): Boolean {

        if (email1.text.toString().isEmpty()) {

            email_layout1?.error = getString(R.string.empty_field)
            email_layout1?.isErrorEnabled = true
            return false

        } else {
            email_layout1?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_password(): Boolean {

        if (password1.text.toString().isEmpty()) {

            password_layout1?.error = getString(R.string.empty_field)
            password_layout1?.isErrorEnabled = true
            return false

        } else {
            password_layout1?.isErrorEnabled = false

        }

        return true

    }
}
