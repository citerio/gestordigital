package cortez.citerio.com.gestordigital;

import android.widget.TextView
import cortez.citerio.com.gestordigital.R.string.company
import android.view.ViewGroup
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.opengl.Visibility
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ImageView


/***** Adapter class extends with ArrayAdapter  */
class SpinnerCustomAdapter
/*************  CustomAdapter Constructor  */
(
    activitySpinner: Payment_Screen,
    textViewResourceId: Int,
    private val data: ArrayList<Payment_Screen.SpinnerModel>,
    var res: Resources
) : ArrayAdapter<Payment_Screen.SpinnerModel>(activitySpinner, textViewResourceId, data) {

    private val activity: Activity
    internal var tempValues: Payment_Screen.SpinnerModel? = null
    internal var inflater: LayoutInflater

    init {

        /********** Take passed values  */
        activity = activitySpinner
        /***********  Layout inflator to call external xml layout ()  */
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getCustomView(position, convertView, parent!!)
    }

    // This funtion called for each row ( Called data.size() times )
    fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {

        /********** Inflate spinner_rows.xml file for each row ( Defined below )  */
        val row = inflater.inflate(R.layout.payment_method_spinner_item, parent, false)

        /***** Get each Model object from Arraylist  */
        tempValues = null
        tempValues = data.get(position) as Payment_Screen.SpinnerModel

        val label = row.findViewById(R.id.bank_name_text) as TextView
        val sub = row.findViewById(R.id.bank_account_text) as TextView
        val bankLogo = row.findViewById(R.id.bank_logo) as ImageView

        if (position == 0) {
            // Default selected Spinner item
            bankLogo.visibility = View.GONE
            label.text = "Seleccione el metodo de pago"
            sub.text = ""
        } else {
            // Set values for spinner each row
            label.setText(tempValues!!.bank_name)
            sub.setText(tempValues!!.user_account)
            bankLogo.setImageResource(res.getIdentifier("cortez.citerio.com.gestordigital:mipmap/" + tempValues!!.bank_logo, null, null))

        }

        return row
    }
}