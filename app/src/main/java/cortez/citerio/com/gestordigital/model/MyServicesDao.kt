package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface MyServicesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addService(service: MyServices)

    @Query("SELECT * FROM myservices WHERE id = :id")
    fun getService(id: Long): MyServices

    @Query("SELECT * FROM myservices WHERE user_id = :user AND provider_id = :provider")
    fun getServicesByProviderByUser(user: Long, provider: Long): List<MyServices>

    @Query("SELECT * FROM myservices WHERE user_id = :user AND provider_type = :provider")
    fun getServicesByProviderTypeByUser(user: Long, provider: Long): List<MyServices>

    @Query("DELETE FROM myservices")
    fun removeAllMyServices()

    @Update
    fun updateService(service: MyServices)

    @Delete
    fun deleteService(service: MyServices)

}