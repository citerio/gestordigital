package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cortez.citerio.com.gestordigital.model.InstitutionRepository

class InstitutionViewModelFactory (private val institutionRepository: InstitutionRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InstitutionViewModel(institutionRepository) as T
    }

}