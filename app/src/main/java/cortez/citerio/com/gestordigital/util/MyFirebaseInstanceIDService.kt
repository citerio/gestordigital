package cortez.citerio.com.gestordigital.util

import com.google.firebase.iid.FirebaseInstanceIdService
import android.preference.PreferenceManager
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import cortez.citerio.com.gestordigital.model.Preference


class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)

        Preference.saveString(applicationContext, "registertoken", refreshedToken)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
        try {
            storeRegistrationId(refreshedToken)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String) {
        // TODO: Implement this method to send token to your app server.
    }

    ///Saving The registration ID
    @Throws(Exception::class)
    private fun storeRegistrationId(token: String?) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val editor = prefs.edit()
        editor.putString(PROPERTY_REG_ID, token)
        editor.apply()

    }

    companion object {

        private val TAG = "MyFirebaseIIDService"
        private val PROPERTY_REG_ID = "reg_id"
    }
}