package cortez.citerio.com.gestordigital

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.android.volley.Request
import com.google.android.material.textfield.TextInputLayout
import com.android.volley.Response
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.create_account_screen.*
import org.json.JSONArray
import org.json.JSONObject




data class User(val name: String="")

class CreateAccount_Screen : AppCompatActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_account_screen)

        name?.addTextChangedListener(MyTextWatcher(name))
        password?.addTextChangedListener(MyTextWatcher(password))
        email?.addTextChangedListener(MyTextWatcher(email))

        ///create account button
        next_step_register_button.setOnClickListener{
            progressbar2.visibility = View.VISIBLE
            next_step_register_button.isEnabled = false

            var valid = true
            if(TextUtils.isEmpty(email.text.toString())){
                email_layout?.error = getString(R.string.empty_field)
                email_layout?.isErrorEnabled = true
                valid = false
            }
            if(TextUtils.isEmpty(password.text.toString())){
                password_layout?.error = getString(R.string.empty_field)
                password_layout?.isErrorEnabled = true
                valid = false
            }
            if(TextUtils.isEmpty(name.text.toString())){
                name_layout?.error = getString(R.string.empty_field)
                name_layout?.isErrorEnabled = true
                valid = false
            }
            if(valid) {
                val params = JSONObject()
                params.put("name", name?.text.toString())
                params.put("email", email?.text.toString())
                params.put("password", password?.text.toString())

                apiController.post(EndPoints.URL_REGISTER, this@CreateAccount_Screen, params,
                    { response ->
                        //progressbar.visibility = View.GONE
                        val result = response?.getString("result")
                        if (result == "OK") {
                            Toast.makeText(this@CreateAccount_Screen, "Usuario Creado Exitosamente",
                                    Toast.LENGTH_SHORT).show();
                            //val intent = Intent(this@CreateAccount_Screen, SecretQuestion_Screen::class.java)
                            val intent = Intent(this@CreateAccount_Screen, SignIn_Screen::class.java)
                            //Log.e("userId", response.getString("userId"))
                            intent.putExtra("email", email?.text.toString())
                            progressbar2.visibility = View.GONE
                            startActivity(intent)
                            finish()
                        } else {
                            progressbar2.visibility = View.GONE
                            next_step_register_button.isEnabled = true
                            Toast.makeText(this@CreateAccount_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }

                    },
                    { error ->
                        val networkResponse = error?.networkResponse
                        var jsonError: String = ""
                        var jsonObject: JSONObject? = null
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = String(networkResponse.data)
                            jsonObject = JSONObject(jsonError)

                            if(jsonObject?.has("errors")) {
                                showServerErrors(jsonObject?.getJSONArray("errors"))
                            } else {
                                Toast.makeText(this@CreateAccount_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            // Print Error!
                        } else {
                            Toast.makeText(this@CreateAccount_Screen, "Ha ocurrido un error. Intente nuevamente",
                                    Toast.LENGTH_SHORT).show();
                        }
                        next_step_register_button.isEnabled = true
                        progressbar2.visibility = View.GONE
                    })

                /*val que = Volley.newRequestQueue(this@CreateAccount_Screen)
                val req = object: StringRequest(Request.Method.POST, EndPoints.URL_REGISTER,
                        Response.Listener {
                            response ->
                            val intent = Intent(this@CreateAccount_Screen, SecretQuestion_Screen::class.java)
                            val response = JSONObject(response)
                            Log.e("userId", response.getString("userId"))
                            intent.putExtra("userId", response.getString("userId"))
                            startActivity(intent)
                            finish()
                        }, Response.ErrorListener {
                            error ->
                            val networkResponse = error.networkResponse
                            var jsonError: String = ""
                            if (networkResponse != null && networkResponse.data != null) {
                                jsonError = String(networkResponse.data)
                                showServerErrors(jsonError)
                                Log.i("response", jsonError)
                                // Print Error!
                            } else {
                                Toast.makeText(this@CreateAccount_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            progressbar2.visibility = View.GONE
                            next_step_register_button.isEnabled = true
                        }) {
                    @Throws(AuthFailureError::class)
                    override fun getParams(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params.put("name", name?.text.toString())
                        params.put("email", email?.text.toString())
                        params.put("password", password?.text.toString())
                        return params
                    }
                }

                que.add(req)*/
            } else {
                progressbar2.visibility = View.GONE
                next_step_register_button.isEnabled = true
            }

        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.

        if(Preference.getString(applicationContext, "usertoken") != null && Preference.getString(applicationContext, "usertoken") != ""){
            val intentToMain = Intent(this@CreateAccount_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            when (view.id) {
                R.id.name -> validate_name()
                R.id.email -> validate_email()
                R.id.password -> validate_password()
            }

        }
    }

    private fun validate_name(): Boolean {

        if (name.text.toString().isEmpty()) {

            name_layout?.error = getString(R.string.empty_field)
            name_layout?.isErrorEnabled = true
            return false

        } else {
            name_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_email(): Boolean {

        if (email.text.toString().isEmpty()) {

            email_layout?.error = getString(R.string.empty_field)
            email_layout?.isErrorEnabled = true
            return false

        } else {
            email_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun validate_password(): Boolean {

        if (password.text.toString().isEmpty()) {

            password_layout?.error = getString(R.string.empty_field)
            password_layout?.isErrorEnabled = true
            return false

        } else {
            password_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun showServerErrors(errors: JSONArray) {
        password_layout?.isErrorEnabled = false
        email_layout?.isErrorEnabled = false
        name_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }
}
