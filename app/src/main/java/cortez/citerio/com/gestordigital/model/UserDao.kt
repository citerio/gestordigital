package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(user: User)

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUser(id: Int): User

    /*@Query("SELECT * FROM user WHERE email = :email AND password = :password")
    fun getUser(email: String, password: String): User*/

    @Query("DELETE FROM user")
    fun removeAllUsers()

    @Update
    fun updateUser(user: User)

}