package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cortez.citerio.com.gestordigital.model.Preference
import kotlinx.android.synthetic.main.detail_invoice_screen.*

class InvoiceDetail_Screen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_invoice_screen)

        provider.text = intent.getStringExtra("name")
        amount.text = intent.getStringExtra("amount")
        number.text = intent.getStringExtra("number")
        issue_date.text = intent.getStringExtra("date")
        expiration_date.text = intent.getStringExtra("dueDate")
        description.text = intent.getStringExtra("description")
    }



    override fun onStart() {

        super.onStart()

        if(Preference.getString(applicationContext, "usertoken") == ""){
            val intentToMain = Intent(this@InvoiceDetail_Screen, SignIn_Screen::class.java)
            intentToMain.putExtra("id", intent.getStringExtra("id"))
            intentToMain.putExtra("name", intent.getStringExtra("name"))
            intentToMain.putExtra("amount", intent.getStringExtra("amount"))
            intentToMain.putExtra("number",intent.getStringExtra("number"))
            intentToMain.putExtra("date", intent.getStringExtra("date"))
            intentToMain.putExtra("dueDate", intent.getStringExtra("dueDate"))
            intentToMain.putExtra("description", intent.getStringExtra("description"))
            intentToMain.putExtra("nextView", "detail")
            startActivity(intentToMain)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@InvoiceDetail_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@InvoiceDetail_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }


}
