package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.ViewModel
import cortez.citerio.com.gestordigital.model.MyInvoiceRepository

class MyInvoiceViewModel internal constructor(private val myInvoiceRepository: MyInvoiceRepository) : ViewModel(){

    fun getAllInvoicesOrderByDate() = myInvoiceRepository.getAllInvoicesOrderByDate()

    fun getInvoicesByStatusOrderByDate(status: Int) = myInvoiceRepository.getInvoicesByStatusOrderByDate(status)

    fun getInvoicesByStatusCount(status: Int) = myInvoiceRepository.getInvoicesByStatusCount(status)
}