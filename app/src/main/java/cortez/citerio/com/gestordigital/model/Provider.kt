package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Provider constructor(
        @PrimaryKey var id: Long = 0,
        var name: String = "",
        var type: Long = 0)
{
}