package cortez.citerio.com.gestordigital

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.forgot_password_screen.*
import org.json.JSONArray
import org.json.JSONObject


class ForgotPassword_Screen : AppCompatActivity() {

    val service = ServiceVolley()
    val apiController = APIController(service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_password_screen)

        email?.addTextChangedListener(MyTextWatcher(email))

        send_code_button?.setOnClickListener{
            progressbar.visibility = View.VISIBLE
            send_code_button.isEnabled = false

            var valid = true
            if(TextUtils.isEmpty(email.text.toString().trim())){
                email_layout?.error = getString(R.string.empty_field)
                email_layout?.isErrorEnabled = true
                valid = false
            }

            if(valid) {
                val params = JSONObject()
                params.put("email", email?.text.toString().trim())

                apiController.post(EndPoints.URL_FORGOT_PASSWORD, this@ForgotPassword_Screen, params,
                        {response ->
                            //progressbar.visibility = View.GONE
                            val result = response?.getString("result")
                            if(result == "OK") {
                                val intent = Intent(this@ForgotPassword_Screen, RetrievePassword_Screen::class.java)
                                intent.putExtra("email", email.text.toString().trim())
                                startActivity(intent)
                                finish()
                            } else {
                                progressbar.visibility = View.GONE
                                send_code_button?.isEnabled = true
                                Toast.makeText(this@ForgotPassword_Screen, "Ha ocurrido un error. Intente nuevamente.",
                                        Toast.LENGTH_SHORT).show();
                            }

                        },
                        {error ->
                            val networkResponse = error?.networkResponse
                            var jsonError: String = ""
                            var jsonObject: JSONObject? = null
                            if (networkResponse != null && networkResponse.data != null) {
                                jsonError = String(networkResponse.data)
                                jsonObject = JSONObject(jsonError)

                                if(jsonObject?.has("errors")) {
                                    showServerErrors(jsonObject?.getJSONArray("errors"))
                                } else {
                                    Toast.makeText(this@ForgotPassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                            Toast.LENGTH_SHORT).show();
                                }
                                // Print Error!
                            } else {
                                Toast.makeText(this@ForgotPassword_Screen, "Ha ocurrido un error. Intente nuevamente",
                                        Toast.LENGTH_SHORT).show();
                            }
                            send_code_button?.isEnabled = true
                            progressbar.visibility = View.GONE
                        })
            } else {
                progressbar.visibility = View.GONE
                send_code_button?.isEnabled = true
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        /*val currentUser = mAuth?.getCurrentUser()

        if(currentUser != null) {
            val intentToMain = Intent(this@SignIn_Screen, Tabs_Screen::class.java)
            startActivity(intentToMain)
            finish()
        }*/
    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (wrong_credentials.isShown){
                wrong_credentials.visibility = View.GONE
            }
        }

        override fun afterTextChanged(s: Editable) {
            Log.e("info", "paso por aqui")
            when (view.id) {
                R.id.email1 -> validate_email()
            }

        }
    }

    private fun validate_email(): Boolean {

        if (email.text.toString().isEmpty()) {

            email_layout?.error = getString(R.string.empty_field)
            email_layout?.isErrorEnabled = true
            return false

        } else {
            email_layout?.isErrorEnabled = false

        }

        return true

    }

    private fun showServerErrors(errors: JSONArray) {
        email_layout?.isErrorEnabled = false

        for (i in 0 until errors!!.length()) {
            val param = errors.getJSONObject(i).getString("param")
            val msg = errors.getJSONObject(i).getString("msg")

            val layout = findViewById<TextInputLayout>(resources.getIdentifier(param+"_layout", "id", packageName))
            layout.error = msg
            layout.isErrorEnabled = true
        }
    }

}
