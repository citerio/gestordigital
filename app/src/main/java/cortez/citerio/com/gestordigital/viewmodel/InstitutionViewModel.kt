package cortez.citerio.com.gestordigital.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cortez.citerio.com.gestordigital.model.InstitutionRepository

class InstitutionViewModel internal constructor(private val institutionRepository: InstitutionRepository) : ViewModel(){

    fun getInstitutionsByCategory(category: String) = institutionRepository.getInstitutionsByCategory(category)
}