package cortez.citerio.com.gestordigital.util

interface LogoutListener {
    fun onSessionLogout()

}