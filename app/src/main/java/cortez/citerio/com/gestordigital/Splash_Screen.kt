package cortez.citerio.com.gestordigital

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.content.Intent
import android.os.Handler
import android.util.Log


class Splash_Screen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val decorView = window.decorView
        val uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.systemUiVisibility = uiOptions

        //Log.i("nota", "${intent.getStringExtra()}")
        Handler().postDelayed( {
            /*if (logged()) {

                val employee_tabs_screen = Intent(this@Splash_Screen, Employee_Tabs_Screen::class.java)
                startActivity(employee_tabs_screen)
                finish()

            } else {

                val logging_screen = Intent(this@Splash_Screen, Logging_Screen::class.java)
                startActivity(logging_screen)
                finish()

            }*/
            val logging_screen = Intent(this@Splash_Screen, Logging_System_Screen::class.java)
            startActivity(logging_screen)
            finish()
        }, 3000)
    }
}
