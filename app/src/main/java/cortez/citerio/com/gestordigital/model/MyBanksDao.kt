package cortez.citerio.com.gestordigital.model

import androidx.room.*


@Dao
interface MyBanksDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addBank(bank: MyBanks)

    @Query("SELECT * FROM mybanks WHERE id = :id")
    fun getBank(id: Int): MyBanks

    @Query("SELECT * FROM mybanks WHERE user_id = :user AND bank_id = :bank")
    fun getBanksByBankByUser(user: Long, bank: Long): List<MyBanks>

    @Query("DELETE FROM mybanks")
    fun removeAllMyBanks()

    @Update
    fun updateBank(bank: MyBanks)

}