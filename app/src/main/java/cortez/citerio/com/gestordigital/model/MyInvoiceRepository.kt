package cortez.citerio.com.gestordigital.model

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import cortez.citerio.com.gestordigital.data.BoundaryCallback
import cortez.citerio.com.gestordigital.volley.ServiceVolley

class MyInvoiceRepository private constructor(private val myInvoicesDao: MyInvoicesDao, private val service: ServiceVolley){


    fun getAllInvoicesOrderByDate(): LiveData<PagedList<InvoiceInst>>{

        // Get data source factory from the local cache
        val dataSourceFactory = myInvoicesDao.getAllInvoicesOrderByDate()

        // every new query creates a new BoundaryCallback
        // The BoundaryCallback will observe when the user reaches to the edges of
        // the list and update the database with extra data
        val boundaryCallback = BoundaryCallback(myInvoicesDao, service)
        val networkErrors = boundaryCallback.networkErrors

        // Get the paged list
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()

        // Get the network errors exposed by the boundary callback
        //return RepoSearchResult(data, networkErrors)
        return data

    }

    fun getInvoicesByStatusOrderByDate(status: Int): LiveData<PagedList<InvoiceInst>>{

        // Get data source factory from the local cache
        val dataSourceFactory = myInvoicesDao.getInvoicesByStatusOrderByDate(status)

        // every new query creates a new BoundaryCallback
        // The BoundaryCallback will observe when the user reaches to the edges of
        // the list and update the database with extra data
        val boundaryCallback = BoundaryCallback(myInvoicesDao, service)
        val networkErrors = boundaryCallback.networkErrors

        // Get the paged list
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()

        // Get the network errors exposed by the boundary callback
        //return RepoSearchResult(data, networkErrors)
        return data


    }

    fun updateInvoicesStatus(id: String, status: Int) =  myInvoicesDao.updateInvoicesStatus(id, status)

    fun getInvoicesByStatusCount(status: Int) = myInvoicesDao.getInvoicesByStatusCount(status)


    companion object {
        private const val DATABASE_PAGE_SIZE = 20
        // For Singleton instantiation
        @Volatile private var instance: MyInvoiceRepository? = null

        fun getInstance(myInvoicesDao: MyInvoicesDao, service: ServiceVolley) =
                instance ?: synchronized(this) {
                    instance ?: MyInvoiceRepository(myInvoicesDao, service).also { instance = it }
                }
    }
}