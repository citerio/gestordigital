package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.MyInstitutionServ
import cortez.citerio.com.gestordigital.model.MyServices
import cortez.citerio.com.gestordigital.model.Preference
import cortez.citerio.com.gestordigital.util.InjectorUtils
import cortez.citerio.com.gestordigital.viewmodel.MyInstitutionViewModel
import kotlinx.android.synthetic.main.affiliate_service_screen.*
import kotlinx.android.synthetic.main.services_screen.*
import kotlinx.android.synthetic.main.toolbar.*
import net.idik.lib.slimadapter.SlimAdapter

class Services_Screen : BaseActivity() {

    /*****************************variables*****************************/

    private val NEW_SERVICE_ADDED = "NEW_SERVICE_ADDED"
    private val TAG = "SistemaVirtualApp"
    private lateinit var viewModel: MyInstitutionViewModel

    /*****************************variables*****************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.services_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        add_service_button.setOnClickListener{
            goToAffiliate()
        }

        val factory = InjectorUtils.provideMyInstitutionViewModelFactory(this@Services_Screen)
        viewModel = ViewModelProviders.of(this, factory).get(MyInstitutionViewModel::class.java)

        show_services()

    }

    fun goToAffiliate(){
        val affiliate_service = Intent(this@Services_Screen, AffiliateService_Screen::class.java)
        affiliate_service.putExtra("categoryId", intent.getStringExtra("categoryId"))
        startActivity(affiliate_service)
    }

    override fun onResume() {
        super.onResume()
        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Services_Screen, SignIn_Screen::class.java)
            val sBuilder = TaskStackBuilder.create(this@Services_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }

        Log.e("entro", "entro en esto")
        //show_services()
    }

    /*****************************showing contacts from db*****************************/
    /*fun show_services(){

        object : AsyncTask<RecyclerView, Void, String>() {


            private var v: RecyclerView? = null
            private var message: String = ""
            private var services: ArrayList<MyServices> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    services = database?.myServicesDao()?.getServicesByProviderTypeByUser(0, intent.getLongExtra("type", 0)) as ArrayList<MyServices>
                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }


            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList(services)

                    if(!(services == null || services.size > 0)){
                        no_services.visibility = View.VISIBLE
                    }else{
                        no_services.visibility = View.INVISIBLE
                    }

                } else {

                    //v!!.visibility = View.GONE
                    no_services.visibility = View.VISIBLE
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, services_list)


    }*/

    /*fun show_services(){

        object : AsyncTask<RecyclerView, Void, String>() {
            private var v: RecyclerView? = null
            private var message: String = ""
            private var services: ArrayList<MyInstitutionServ> = ArrayList()

            override fun doInBackground(vararg params: RecyclerView): String? {

                v = params[0]

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    services = database?.myInstitutionsDao()?.getMyInstitutionsByCategory(intent.getStringExtra("categoryId")) as ArrayList<MyInstitutionServ>
                    message = "success"
                    return message

                }catch (e:Exception){
                    e.printStackTrace()
                    message = "failure"
                    return message
                }


            }

            override fun onPostExecute(message: String?) {
                super.onPostExecute(message)

                if (message.equals("success")) {

                    refreshList(services)

                    if(!(services == null || services.size > 0)){
                        no_services.visibility = View.VISIBLE
                    }else{
                        no_services.visibility = View.INVISIBLE
                    }

                } else {

                    //v!!.visibility = View.GONE
                    no_services.visibility = View.VISIBLE
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, services_list)
    }*/

    /*/*****************************receiving new contacts in db and updating contacts UI*****************************/
    private inner class ServiceBroadCastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val local = intent.extras!!.getString("RESULT_CODE")!!
            when(local){
                NEW_SERVICE_ADDED -> show_services()
            }

        }
    }*/

    /*****************************updating contacts UI*****************************/
    /*fun updateServices() {
        val contacts = contactQuery.find()
        adapter?.setContacts(contacts = contacts as ArrayList<Contact>)
        val items = adapter?.itemCount
        if(!(items == null || items <= 0)){
            no_contacts_?.visibility = View.INVISIBLE
        }
        Log.v(TAG, "update contacts called")
    }*/

    fun show_services(){
        viewModel.getMyInstitutionsByCategory(intent.getStringExtra("categoryId")).observe(this@Services_Screen, Observer { inst ->
            if (inst != null){
                adapter.updateData(inst).notifyDataSetChanged()
                if(inst.isEmpty()){
                    no_services.visibility = View.VISIBLE
                }else{
                    no_services.visibility = View.INVISIBLE
                }
            }
        })
    }


    fun refreshList(services: ArrayList<MyInstitutionServ>){

        Log.v(TAG, "refreshList called")

        adapter.updateData(services).notifyDataSetChanged()
    }

    /*private val adapter by lazy {
        SlimAdapter.create()
                .register<MyServices>(R.layout.my_provider) { data, injector ->
                    injector
                            .text(R.id.provider_name, data.provider_name)
                            .text(R.id.provider_fl, data.provider_name.first().toUpperCase().toString())
                            .clicked(R.id.see_button){
                                val intent = Intent(this@Services_Screen, Service_See_Delete_Screen::class.java)
                                intent.putExtra("id", data.id)
                                startActivity(intent)
                            }
                }
                .attachTo(recyclerView)
    }*/

    private val adapter by lazy {
        SlimAdapter.create()
                .register<MyInstitutionServ>(R.layout.my_provider) { data, injector ->
                    injector
                            .text(R.id.provider_name, data.instName)
                            .text(R.id.provider_fl, data.instName.first().toUpperCase().toString())
                            .clicked(R.id.see_button){
                                val intent = Intent(this@Services_Screen, Service_See_Delete_Screen::class.java)
                                intent.putExtra("id", data.id)
                                startActivity(intent)
                            }
                }
                .attachTo(recyclerView)
    }

    private val recyclerView: RecyclerView by lazy<RecyclerView> {
        services_list.apply { // uxStores is the recyclerview’s name
            layoutManager = LinearLayoutManager(baseContext, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(this@Services_Screen, LinearLayoutManager.VERTICAL))

        }!!
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.getItemId()) {
            R.id.add_button -> {
                goToAffiliate()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }



}
