package cortez.citerio.com.gestordigital

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

internal class CardsSlideAdapter(private val context: Context, private val imagesArrayList: ArrayList<Int>) : PagerAdapter() {

    private val inflater =  LayoutInflater.from(context)

    override fun getCount(): Int {
        return imagesArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.cards_image_slide, view, false)!!

        val imageView = imageLayout
                .findViewById(R.id.cards_slider_image_view) as ImageView


        imageView.setImageResource(imagesArrayList[position])

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}