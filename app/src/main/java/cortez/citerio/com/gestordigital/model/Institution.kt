package cortez.citerio.com.gestordigital.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = arrayOf(ForeignKey(entity = Category::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("category_id"),
        onDelete = ForeignKey.CASCADE)))
data class Institution constructor(
        @PrimaryKey val id: String,
        var name: String = "",
        var category_id: String = "")
{
}