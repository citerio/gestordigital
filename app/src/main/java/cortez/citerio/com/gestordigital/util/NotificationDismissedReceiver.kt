package cortez.citerio.com.gestordigital.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.gson.Gson
import cortez.citerio.com.gestordigital.model.Notification
import cortez.citerio.com.gestordigital.model.Preference

class NotificationDismissedReceiver : BroadcastReceiver() {

    private val TAG = "SistemaVirtualApp"

    override fun onReceive(context: Context, intent: Intent) {

        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        //TODO("NotificationDismissedReceiver.onReceive() is not implemented")
        Log.v(TAG, "onReceive NotificationDismissedcalled")
        val notificationArrayList = ArrayList<Notification>()
        val notificationGsonList = Gson().toJson(notificationArrayList)
        Preference.saveString(context,"notificationList", notificationGsonList)
    }


}
