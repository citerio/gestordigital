package cortez.citerio.com.gestordigital

import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.graphics.*
import android.os.AsyncTask
import com.google.android.material.textfield.TextInputLayout
import androidx.appcompat.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.widget.*
import cortez.citerio.com.gestordigital.model.AppDatabase
import cortez.citerio.com.gestordigital.model.Preference
import kotlinx.android.synthetic.main.payment_screen.*
import cortez.citerio.com.gestordigital.volley.APIController
import cortez.citerio.com.gestordigital.volley.ServiceVolley
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject


class Payment_Screen : BaseActivity() {

    /*****************************variables*****************************/
    private val TAG = "SistemaVirtualApp"
    //var CustomListViewValuesArr = ArrayList<SpinnerModel>()
    val paymentMethodsList = ArrayList<PaymentMethod>()
    var paymentMethodImageViewList = ArrayList<ImageView>()
    var adapter: SpinnerCustomAdapter? = null
    var answer_layout_ : TextInputLayout? = null
    var answer_: EditText? = null
    val service = ServiceVolley()
    val apiController = APIController(service)
    var invoiceId = ""
    var securityQuestion: JSONObject? = null
    var dialog: AlertDialog? = null

    private var selectedPaymentMethod = ""
    /*****************************variables*****************************/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_screen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        //setListData()

        invoiceId = intent.getStringExtra("id")
        Log.v(TAG, "notification on Payment Screen: " + intent.getIntExtra("notification", -1))
        /*NotificationManagerCompat.from(this).apply {
            cancel(intent.getIntExtra("notification", -1))
        }*/
        clearNotifications()


        setViewObjects(intent)
        //populatePaymentMethodsList()
        //addPaymentMethodsToView()

        /*adapter = SpinnerCustomAdapter(this, R.layout.payment_method_spinner_item, CustomListViewValuesArr, resources)
        spinner1.adapter = adapter

        spinner1.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, v: View, position: Int, id: Long) {
                // your code here

                // Get selected row data to show on screen
                val bank_name = (v.findViewById<View>(R.id.bank_name_text) as TextView).text.toString()
                val bank_account = (v.findViewById<View>(R.id.bank_account_text) as TextView).text.toString()
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        })


        cards_slider.adapter = CardsSlideAdapter(this@Payment_Screen, myImageList)*/

        /*pay_button.setOnClickListener{
            when(selectedPaymentMethod) {
                "" -> {
                    Toast.makeText(this@Payment_Screen, "Debe seleccionar un metodo de pago",
                            Toast.LENGTH_SHORT).show();
                }
                else -> {
                    secretQuestionDialog()
                }
            }
        }*/

        pay_button.setOnClickListener{
            val affiliate_tc = Intent(this@Payment_Screen, New_TC_Screen::class.java)
            affiliate_tc.putExtra("invoiceId", intent.getStringExtra("id"))
            affiliate_tc.putExtra("name", intent.getStringExtra("name"))
            startActivity(affiliate_tc)
            finish()
        }

        answer_?.addTextChangedListener(MyTextWatcher(answer_!!))

    }


    private fun setViewObjects(intent: Intent) {
        val name = intent.getStringExtra("name")
        val amount = intent.getStringExtra("amount")
        /*val category = intent.getIntExtra("category", 0)

        var icon = ""
        var instCat = ""
        when(category) {
            1 ->  {
                instCat = getString(R.string.school)
                icon = "baseline_school_black_36"
            }
            2 -> {
                instCat = getString(R.string.university)
                icon = "baseline_account_balance_black_36"
            }
            3 -> {
                instCat = getString(R.string.gym)
                icon = "baseline_fitness_center_black_36"
            }
            4 -> {
                instCat = getString(R.string.electricity)
                icon = "baseline_highlight_black_36"
            }
            5 -> {
                instCat = getString(R.string.water)
                icon = "baseline_local_drink_black_36"
            }
            else -> {
                instCat = getString(R.string.cable_tv)
                icon = "baseline_live_tv_black_36"
            }
        }*/

        //category_image.setImageResource(resources.getIdentifier("cortez.citerio.com.gestordigital:mipmap/" + icon, null, null))
        //category_text.text = instCat
        name_institute.text = name
        amount_text.text = amount
        number.text = intent.getStringExtra("number")
        issue_date.text = intent.getStringExtra("date")
        expiration_date.text = intent.getStringExtra("dueDate")
        description.text = intent.getStringExtra("description")
    }

    private fun populatePaymentMethodsList() {
        /*val item1 = "visa"
        val item2 = "banesco"

        paymentMethodsList.add(item1)
        paymentMethodsList.add(PaymentMethod("asfd", "a127877234234888", "visa"))
        addPaymentMethodsToView()*/
        apiController.get(EndPoints.URL_PAYMENT_METHOD, applicationContext,
            {response ->
                val paymentMethods = response?.getJSONArray("paymentMethods")
                Log.i("tarjetas", "$paymentMethods")
                for (i in 0 until paymentMethods!!.length()) {
                    val id = paymentMethods.getJSONObject(i)?.getString("_id")
                    val cardNumber = paymentMethods.getJSONObject(i)?.getString("cardNumber")
                    val type = paymentMethods.getJSONObject(i)?.getString("type")

                    paymentMethodsList.add(PaymentMethod(id!!, cardNumber!!, type!!))
                }
                addPaymentMethodsToView()
            },
            {error ->
                Toast.makeText(applicationContext, "Ha ocurrido un error. Intente nuevamente",
                    Toast.LENGTH_SHORT).show();
            })
    }

    private fun addPaymentMethodsToView() {
        for(i in 0 until paymentMethodsList.size) {
            val scale = resources.displayMetrics.density;

            val paint = Paint()
            paint.color = Color.WHITE // Text Color
            paint.textSize =  (14 * scale); // Text Size
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)

            var bitmap = BitmapFactory.decodeResource(resources, resources.getIdentifier("cortez.citerio.com.gestordigital:mipmap/tarj_${paymentMethodsList[i].type}", null, null));
            var bitmapConfig = bitmap.config

            if(bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true);

            val canvas: Canvas = Canvas(bitmap)
            canvas.drawBitmap(bitmap, 0f, 0f, paint)


            //canvas.drawText("**** " + "**** " + "**** " + paymentMethodsList[i].cardNumber.takeLast(4), 80f, 50f, paint)
            canvas.drawText( paymentMethodsList[i].cardNumber.takeLast(4), 60f, 25f, paint)

            val imageView = ImageView(this)

            //imageView.draw(canvas)
            imageView.setImageBitmap(bitmap)
            imageView.tag = paymentMethodsList[i].id
            //imageView.setImageResource(resources.getIdentifier("cortez.citerio.com.gestordigital:mipmap/tarj_${paymentMethodsList[i].type}", null, null))
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            imageView.layoutParams = params
            imageView.setPadding(0, 0, 8 ,0)

            paymentMethodImageViewList.add(imageView)
            imageView.setOnClickListener {
                if(it.tag.toString() == this.selectedPaymentMethod) {
                    this.selectedPaymentMethod = ""
                    it.setBackgroundResource(android.R.color.transparent)
                    pay_button.setBackgroundResource(R.color.colorGreyL2X)
                } else {
                    paymentMethodImageViewList.forEach {
                        it.setBackgroundResource(android.R.color.transparent)
                    }
                    this.selectedPaymentMethod = it.tag.toString()
                    Log.i("selected", this.selectedPaymentMethod)
                    it.setBackgroundResource(R.drawable.rounded_button)
                    pay_button.setBackgroundResource(R.color.colorPrimary)
                }
            }

            layout_card_slider.addView(imageView)
        }
    }

    fun secretQuestionDialog(){
        progressbar.visibility = View.VISIBLE
        val dialogView = layoutInflater.inflate(R.layout.secret_question_confirmation, null)
        val alert = AlertDialog.Builder(this@Payment_Screen)
        alert.setView(dialogView)
        dialog = alert.create()
        answer_layout_ = dialogView.findViewById(R.id.answer_layout)
        answer_ = dialogView.findViewById(R.id.answer)
        dialog?.setCancelable(false)
        dialogView.findViewById<Button>(R.id.no).setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.yes).setOnClickListener {
            val params = JSONObject()
            params.put("questionId", securityQuestion?.getString("_id") )
            params.put("answer", answer_?.text.toString())

            apiController.post(EndPoints.URL_CHECK_ANSWER, this@Payment_Screen, params,
                {response ->
                    //progressbar.visibility = View.GONE
                    val result = response?.getString("result")
                    if(result == "OK") {
                        makePayment()
                    } else {
                        answer_layout_?.error = getString(R.string.wrong_answer)
                        answer_layout_?.isErrorEnabled = true
                    }

                },
                {error ->
                    Toast.makeText(this@Payment_Screen, "Ha ocurrido un error. Intente nuevamente.",
                            Toast.LENGTH_SHORT).show();
                })
        }

        apiController.get(EndPoints.URL_GET_SQ_USER, applicationContext,
            {response ->
                securityQuestion = response?.getJSONObject("securityQuestion")
                answer_layout_?.hint = securityQuestion?.getString("question")
                progressbar.visibility = View.GONE
                dialog?.show()
            },
            {error ->
                progressbar.visibility = View.GONE
                Toast.makeText(this@Payment_Screen, "Ha ocurrido un error. Intente nuevamente.",
                        Toast.LENGTH_SHORT).show();
            })
    }

    private fun makePayment() {
        val payParams = JSONObject()
        payParams.put("invoiceId", intent.getStringExtra("id"))
        apiController.post(EndPoints.URL_PAY_INVOICE, applicationContext, payParams,
            {response ->
                dialog?.dismiss()
                updateInvoiceStatus()
                //showDialogSuccess()
            },
            {error ->
                dialog?.dismiss()
                Toast.makeText(this@Payment_Screen, "Ha ocurrido un error mientras se ejecutaba el pago. Intente nuevamente.",
                        Toast.LENGTH_SHORT).show();
            })
    }

    private fun updateInvoiceStatus() {
        object : AsyncTask<String, Void, String>() {


            private var message = ""


            override fun doInBackground(vararg params: String): String {

                try {

                    val database = AppDatabase.getDatabase(context = baseContext)
                    database?.myInvoicesDao()?.updateInvoicesStatus(intent.getStringExtra("id"), 1)

                    message = "success";
                    return message;

                } catch (e: Exception) {

                    e.printStackTrace()
                    message = "failure";
                    return message;

                }
            }

            override fun onPostExecute(m: String) {
                super.onPostExecute(m)

                if (m.equals("success")) {
                    try {
                        Log.v(TAG, "Invoice updated")
                        showDialogSuccess()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {

                    Log.v(TAG, "Error on updating invoice")
                    Toast.makeText(this@Payment_Screen, "Error realizando el pago.", Toast.LENGTH_LONG).show()

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "")
    }

    private fun showDialogSuccess() {
        val dialogView = layoutInflater.inflate(R.layout.success_payment, null)
        val alert = AlertDialog.Builder(this@Payment_Screen)
        alert.setView(dialogView)
        val dialog = alert.create()
        dialog.setCancelable(false)

        val message = dialogView.findViewById<TextView>(R.id.message)

        message?.text = getString(R.string.up_to_date) + " " + intent.getStringExtra("name")
        dialogView.findViewById<Button>(R.id.accept).setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@Payment_Screen, Tabs_Screen::class.java)
            startActivity(intent)
            finish()
        }
        dialog.show()
    }


    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            when (view.id) {
                answer_?.id -> validate_answer1()
            }

        }
    }

    private fun validate_answer1(): Boolean {

        if (answer_?.text.toString().isEmpty()) {
            answer_layout_?.error = getString(R.string.empty_field)
            answer_layout_?.isErrorEnabled = true
            return false

        } else {
            answer_layout_?.isErrorEnabled = false

        }

        return true

    }


    override fun onStart() {
        super.onStart()

        if(Preference.getString(applicationContext, "usertoken").isEmpty()){
            val intentToMain = Intent(this@Payment_Screen, SignIn_Screen::class.java)
            intentToMain.putExtra("id", intent.getStringExtra("id"))
            intentToMain.putExtra("category", intent.getIntExtra("category", 0))
            intentToMain.putExtra("name", intent.getStringExtra("name"))
            intentToMain.putExtra("amount", intent.getStringExtra("amount"))
            intentToMain.putExtra("number",intent.getStringExtra("number"))
            intentToMain.putExtra("date", intent.getStringExtra("date"))
            intentToMain.putExtra("dueDate", intent.getStringExtra("dueDate"))
            intentToMain.putExtra("description", intent.getStringExtra("description"))
            intentToMain.putExtra("nextView", "payment")
            val sBuilder = TaskStackBuilder.create(this@Payment_Screen)
            sBuilder.addNextIntentWithParentStack(intentToMain)
            sBuilder.startActivities()
            finish()
        }
    }


    /*****************************receiving finish() command from baseactivity*****************************/

    /*fun setListData() {

        // Now i have taken static values by loop.
        // For further inhancement we can take data by webservice / json / xml;

        val item: SpinnerModel = SpinnerModel("bancolombia", "Bancolombia", "01020146240000263245")
        val item1: SpinnerModel = SpinnerModel("bancolombia", "Bancolombia", "01020146240000263245")
        val item2: SpinnerModel = SpinnerModel("banesco", "Banesco", "01340026788007722654")
        val item3: SpinnerModel = SpinnerModel("provincial", "Provincial", "01080078430078110020")

        CustomListViewValuesArr.add(item1)
        CustomListViewValuesArr.add(item1)
        CustomListViewValuesArr.add(item2)
        CustomListViewValuesArr.add(item3)
        /*for (i in 0..3) {

            val item = SpinnerModel()

            /******* Firstly take data in model object  */
            item.bank_name = "Bancolombia"
            item.user_account = "01020146240000263245"
            item.bank_logo = "bancolombia"

            /******** Take Model Object in ArrayList  */
            CustomListViewValuesArr.add(item)
        }*/

    }*/

    data class PaymentMethod constructor(
            var id: String = "",
            var cardNumber: String = "",
            var type: String = "",
            var cvc: String = "",
            var expirationDate: String = ""
    )
    {

    }

    data class SpinnerModel constructor(
            var bank_logo: String = "",
            var bank_name: String = "",
            var user_account: String = ""
    )
    {
    }
}
